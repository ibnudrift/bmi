<section class="home-sec-1">
  <div class="py-5"></div>
  <div class="py-4 d-none d-sm-block"></div>
   <div class="prelative container">
    
      <div class="row">
        <div class="col-md-60">
           <div class="judul">
              <h3><?php echo $this->setting['home1_title'] ?></h3>
              <div class="isi">
                 <?php echo $this->setting['home1_subcontent'] ?>
              </div>
           </div>
         </div>
      </div>

      <?php 
        $dn_productlists = [
                              [
                                  'pict'=>'st-prds-1.jpg',
                                  'titles'=>'Shrimp',
                                  'd_info' => 'CEZP, Shrimp Ring, CPND CPTO, HLSO, REZP, RPDTO RPND, HOSO',
                              ],
                              [
                                  'pict'=>'st-prds-2.jpg',
                                  'titles'=>'Fish',
                                  'd_info' => 'Yellow Fin Tuna, Red Snapper, Grouper, Emperor Fish, Parrot Fish',
                              ],
                              [
                                  'pict'=>'st-prds-3.jpg',
                                  'titles'=>'Crab',
                                  'd_info' => 'Pasteurized Blue Swimming Crab – Chilled, Pasteurized Blue Swimming Crab – Frozen, Soft Shell Crab',
                              ],
                              [
                                  'pict'=>'st-prds-4.jpg',
                                  'titles'=>'Cephalopod',
                                  'd_info' => 'Octopus, Cuttlefish, Squid',
                              ],
                              [
                                  'pict'=>'st-prds-5.jpg',
                                  'titles'=>'Value Added',
                                  'd_info' => 'Breaded, Marinated, Coated, Custom Processing, Pre-Fried, Fried',
                              ],
                          ];
      ?>

      <div class="row">
        <?php foreach ($dn_productlists as $key => $value): ?>
         <div class="col-md-12 col-60">
            <div class="box-content">
               <div class="image">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>">
                 <img class="w-100" src="<?php echo Yii::app()->baseUrl. '/asset/images/'. $value['pict']; ?>" alt="">
                 </a>
             </div>
               <div class="content pt-4">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>"><p><?php echo $value['titles'] ?></p></a>
               </div>
            </div>
         </div>
        <?php endforeach ?>
       </div>
       
       <?php /*
        $criteria = new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('t.type = :type');
        $criteria->params[':type'] = 'category';
        // $criteria->limit = 3;
        $criteria->order = 't.sort ASC';
        $nCategory = PrdCategory::model()->findAll($criteria);
       ?>
       <div class="row">
        <?php foreach ($nCategory as $key => $value): ?>
         <div class="col-md-15 col-30">
            <div class="box-content">
               <div class="image">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'category'=> $value->id)); ?>">
                 <img class="w-100" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(298,298, '/images/category/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                 </a>
             </div>
               <div class="content pt-4">
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'category'=> $value->id)); ?>"><p><?php echo $value->description->name ?></p></a>
               </div>
            </div>
         </div>
        <?php endforeach ?>
       </div>
       */ ?>

   </div>
</section>
<div class="view_buttns_allprod">
   <div class="prelative container">
      <div class="row justify-content-center text-center">
        <div class="col-md-60">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>">View All Product</a>
         </div>
      </div>
   </div>
</div>
<section class="home-sec-2" style="background-image: url('<?php echo $this->assetBaseurl; ?>design2_02.jpg'); padding: 115px;">
   <div class="prelative container">
      <div class="row justify-content-center text-center">
        <div class="col-md-60">
          <img src="<?php echo $this->assetBaseurl; ?>Layer-23-copy-2.png" class="img img-fluid" alt="">
        </div>
      </div>
      <div class="content">
         <h3><?php echo $this->setting['home2_chl_title_top'] ?></h3>
      </div>

      <div class="row">
        <?php for ($i=1; $i < 5; $i++) { ?>
         <div class="col-md-15 col-30">
            <div class="isi">
               <h3><?php echo $this->setting['home2_chl_number_'. $i] ?></h3>
               <p><?php echo $this->setting['home2_chl_txt_'. $i] ?></p>
            </div>
         </div>
        <?php } ?>
      </div>

   </div>
</section>

<?php 
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->order = 't.date_input DESC';
$criteria->limit = 3;
$data_blog = Blog::model()->findAll($criteria);
?>
<section class="home-sec-3 outers_block_articleshome">
   <div class="prelative container">
      <h4>Latest Blog & Articles</h4>

      <div class="box-image outers_list_news_data">
         <div class="row no-gutters lists_data">
        <?php foreach ($data_blog as $key => $value): ?>
            <div class="col-md-20">
              <div class="items">
                 <div class="pictures">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(432,261, '/images/blog/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" class="img img-fluid" alt=""></a>
                 </div>
                 <div class="contents">
                    <p><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><?php echo $value->description->title; ?></a></p>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
        <?php endforeach ?>          
         </div>
      </div>

   </div>
</section>

<div class="more">
   <div class="prelative container">
      <div class="row justify-content-center text-center">
        <div class="col-md-21"></div>
        <div class="col-md-18">
          <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">More Blog & Articles</a>
         </div>
        <div class="col-md-21"></div>
      </div>
   </div>
</div>

<style type="text/css">
  .cols_buttons_nav{
    z-index: 200;
  }
</style>