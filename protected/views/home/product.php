<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>cover-product.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Products</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a onclick="window.history.back();" href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

 <section class="home-sec-products">
   <div class="prelatife container">
        <div class="row">
            <div class="col-md-60">
                 <div class="inner-judul">
                   <h3><?php echo $this->setting['home1_title'] ?></h3>
                   <?php echo $this->setting['home1_subcontent'] ?>
                 </div>
             </div>
        </div>
        <div class="py-2"></div>
        <?php 
        $dn_productlists = [
                                [
                                    'pict'=>'st-prds-1.jpg',
                                    'titles'=>'Shrimp',
                                    'd_info' => 'CEZP, Shrimp Ring, CPND CPTO, HLSO, REZP, RPDTO RPND, HOSO',
                                ],
                                [
                                    'pict'=>'st-prds-2.jpg',
                                    'titles'=>'Fish',
                                    'd_info' => 'Yellow Fin Tuna, Red Snapper, Grouper, Emperor Fish, Parrot Fish',
                                ],
                                [
                                    'pict'=>'st-prds-3.jpg',
                                    'titles'=>'Crab',
                                    'd_info' => 'Pasteurized Blue Swimming Crab – Chilled, Pasteurized Blue Swimming Crab – Frozen, Soft Shell Crab',
                                ],
                                [
                                    'pict'=>'st-prds-4.jpg',
                                    'titles'=>'Cephalopod',
                                    'd_info' => 'Octopus, Cuttlefish, Squid',
                                ],
                                [
                                    'pict'=>'st-prds-5.jpg',
                                    'titles'=>'Value Added',
                                    'd_info' => 'Breaded, Marinated, Coated, Custom Processing, Pre-Fried, Fried',
                                ],
                            ];
        ?>

         <div class="outers_list_product">
            <div class="row">
                <?php foreach ($dn_productlists as $key => $value): ?>
                 <div class="col-md-12">
                        <div class="items_box">
                           <div class="picture">
                            <a href="">
                             <img class="w-100" src="<?php echo Yii::app()->baseUrl. '/asset/images/'. $value['pict']; ?>" alt="">
                             </a>
                         </div>
                        <div class="info">
                            <a href=""><h3><?php echo $value['titles'] ?></h3></a>
                            <div class="py-2"></div>
                            <div class="mx-4 subs_info">
                                <p><span>PRODUCT VARIANTS:</span> <br><?php echo $value['d_info'] ?></p>
                            </div>
                        </div>
                    </div>
                 </div>
                <?php endforeach ?>
            </div>
        </div>


        <?php
        /*
        $criteria = new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('t.type = :type');
        $criteria->params[':type'] = 'category';
        $criteria->order = 't.sort ASC';
        $nCategory = PrdCategory::model()->findAll($criteria);
       ?>
        <div class="outers_list_product">
            <div class="row">
                <?php foreach ($nCategory as $key => $value): ?>
                 <div class="col-md-15 col-30">
                    <div class="items_box">
                       <div class="picture">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'category'=> $value->id)); ?>">
                         <img class="w-100" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(298,298, '/images/category/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="">
                         </a>
                     </div>
                    <div class="info">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'category'=> $value->id)); ?>"><h3><?php echo $value->description->name ?></h3></a>
                       </div>
                    </div>
                 </div>
                <?php endforeach ?>
            </div>
        </div>
        */ ?>

        <div class="py-3"></div>
        <div class="clear"></div>
    </div>
</section>