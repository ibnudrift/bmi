<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife">
    	<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,735, '/images/static/'. $this->setting['contact_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
    </div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1><?php echo $this->setting['contact_hero_title'] ?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $this->setting['contact_hero_title'] ?></li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a onclick="window.history.back();" href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="home-sec-contact">
		<div class="prelative container w904">
			<div class="py-4"></div>

			<div class="row">
				<div class="col-md-60">
					<div class="judul">
						<h4>PT. Bumi Menara Internusa</h4>
					</div>
				</div>
			</div>
			
			<div class="row box_info_contacttop">
				<div class="col-md-20">
					<div class="box_item text-center">
						<div class="n_icon text-center"><i class="fa fa-whatsapp"></i></div>
						<div class="texts pt-2">
							<p>Whatsapp<br><a href="https://wa.me/<?php echo $this->wa_call_link ?>"><?php echo $this->wa_call ?> (Click to chat)</a></p>
						</div>
					</div>
				</div>
				<div class="col-md-20">
					<div class="box_item text-center">
						<div class="n_icon text-center"><i class="fa fa-phone"></i></div>
						<div class="texts pt-2">
							<p>Telephone<br><a href="tel:<?php echo str_replace(' ', '', $this->setting['contact_phone']) ?>"><?php echo $this->setting['contact_phone'] ?></a></p>
						</div>
					</div>
				</div>
				<div class="col-md-20">
					<div class="box_item text-center">
						<div class="n_icon text-center"><i class="fa fa-envelope"></i></div>
						<div class="texts pt-2">
							<p>Email<br> <a href="mailto:<?php echo $this->setting['contact_email'] ?>"><?php echo $this->setting['contact_email'] ?></a></p>
						</div>	
					</div>
				</div>
			</div>
			<div class="py-4"></div>
			<div class="py-2"></div>

		</div>
	</section>

	<section class="home-sec-contact-2">
		<div class="prelative container w904">
			<div class="row">
				<?php for ($i=1; $i < 3; $i++) { ?>
				<div class="col-md-30">
					<div class="box-image">
						<img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['contact'.$i.'_pict'] ?>" alt="">
					</div>
					<div class="titles">
						<?php echo $this->setting['contact'.$i.'_desc'] ?>
					</div>
					<?php if ($i == 1): ?>
					<div class="py-3"></div>
					<?php endif ?>
				</div>
				<?php } ?>

				</div>
			</div>
		</div>
	</section>