<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<header class="head <?php /*if ($active_menu_pg != 'home/index'): ?>insides-head<?php endif;*/ ?>">
  <div class="prelative container mx-auto">
    <div class="row">
      <div class="col-md-35 col-30">
        <div class="lgo_header">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">
          <img src="<?php echo $this->assetBaseurl; ?>logo-bmi.svg" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
          </a>
        </div>
        <div class="taglines_hd pl-2">
          <img src="<?php echo $this->assetBaseurl; ?>taglines-hd.jpg" alt="<?php echo 'tagline '.Yii::app()->name; ?>" class="img img-fluid">
        </div>
      </div>
      <div class="col-md-25 col-30">
        <div class="py-2 my-1"></div>
        <div class="text-right tops_menu_linehd">
          <a href="javascript:;" class="showmenu_barresponsive"><img src="<?php echo $this->assetBaseurl; ?>bc_menutag.jpg" alt=""></a>
          <div class="clear clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="outer-blok-black-menuresponss-hides" style="display: none;">
  <div class="prelatife container">
    <div class="clear height-45"></div>
    <div class="float-right mb-3">
      <div class="hidesmenu-frightd"><a href="#" class="closemrespobtn"><img src="<?php echo $this->assetBaseurl ?>closen-btn.png" alt=""></a></div>
    </div>
    <div class="blocksn_logo-centers">
      <img src="<?php echo $this->assetBaseurl ?>logo_popup_head.png" alt="logo" class="img img-fluid d-block mx-auto">
    </div>
    <div class="height-30 hidden-xs"></div>
    <div class="menu-sheader-datals py-4">
      <ul class="list-unstyled">
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>">Our Products</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Our Quality</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blogs & Articles</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
      </ul>
    </div>
    <div class="blocks-info-menubtm d-block mx-auto">
      <span>Need Information?</span>
      <div class="py-2 my-1"></div>
      <div class="row justify-content-center">
        <div class="col-md-30 col-sm-30 col-30">
          <i class="fa fa-whatsapp"></i>
          <div class="py-1"></div>
          <p>Click to Whatsapp Chat<br>
          <a href="https://wa.me/<?php echo $this->wa_call_link ?>"><?php echo $this->wa_call ?></a></p>
        </div>
        <div class="col-md-30 col-sm-30 col-30">
          <i class="fa fa-envelope-o"></i>
          <div class="py-1"></div>
          <p>Send Us Email <br>
          <a href="mailto:<?php echo $this->setting['contact_email'] ?>"><?php echo $this->setting['contact_email'] ?></a></p>
        </div>
      </div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
  $(function(){
    // show and hide menu responsive
    $('a.showmenu_barresponsive').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideToggle('slow');
      return false;
    });
    $('a.closemrespobtn').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideUp('slow');
      return false;
    });

  });
</script>