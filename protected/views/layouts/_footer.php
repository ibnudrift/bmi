<div class="btn-back">
    <hr>
    <a class="t-backtop" href="#">Back To Top</a>
    <hr>
</div>
<section class="footer">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60 justify-content-center text-center">
                <img src="<?php echo $this->assetBaseurl; ?>logo.png" class="img img-fluid" alt="">
            </div>
        </div>
        <div class="py-4"></div>
        <ul class="list-inline">
            <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a>
            </li>
            <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a>
            </li>
            <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/product')); ?>">Our Products</a>
            </li>
            <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Our Quality</a>
            </li>
            <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blogs & Articles</a>
            </li>
            <li class="list-inline-item menu">
                <a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a>
            </li>
        </ul>
    </div>
    <div class="contact">
        <div class="prelative container">
            <div class="row no-gutters">
                <div class="col-md-20">
                    <div class="wa">
                        <p>Whatsapp <a href="https://wa.me/<?php echo $this->wa_call_link ?>"><?php echo $this->wa_call ?></a></p>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="telpon">
                        <p>Telephone <?php echo $this->setting['contact_phone'] ?></p>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="email">
                        <p>Email <a href="mailto:<?php echo $this->setting['contact_email'] ?>"><?php echo $this->setting['contact_email'] ?></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer-2">
    <div class="prelative container">
        <div class="copy">
            <p>Copyright &copy; PT. Bumi Menara Internusa, Al rights reserved - 2019</p>
        </div>
        <p>Responsive Website Design and Development by <a href="https://www.markdesign.net" target="_blank" title="Website Design Surabaya">Mark Design Indonesia</a></p>
    </div>
</section>