<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>cover-blog.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Blogs & Articles</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blogs & Articles</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="home-sec-blog">
  <div class="prelative container">
      <div class="py-4"></div>
      
      <div class="judul titles_pg">
          <h4>Latest Blog & Articles</h4>
      </div>     

      <div class="py-3"></div>
      

      <div class="box-image outers_list_news_data">

         <div class="row no-gutters lists_data">
            <?php foreach ($dataBlog->getData() as $key => $value): ?>
            <div class="col-md-20 col-60">
              <div class="items">
                 <div class="pictures">
                   <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(432,261, '/images/blog/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" class="img img-fluid" alt=""></a>
                 </div>
                 <div class="contents">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><p><?php echo $value->description->title; ?></p></a>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <?php endforeach ?>

         </div>
         
         <div class="py-4"></div>
         <?php $this->widget('CLinkPager', array(
                  'pages' => $dataBlog->getPagination(),
                  'header'=>'',
                  'nextPageLabel'=>'',
                  'prevPageLabel'=>'',
                  'maxButtonCount'=>5,
                  // 'cssFile'=>Yii::app()->baseUrl.'/asset/css/style-pager.css',
              )) ?>

      </div>
      <div class="clear clearfix"></div>

      <div class="clear clearfix"></div>
    </div>
</section>