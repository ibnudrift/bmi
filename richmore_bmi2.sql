-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 12, 2020 at 11:45 PM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richmore_bmi2`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(25) NOT NULL,
  `category_id` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `address_1` varchar(100) NOT NULL,
  `address_2` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `category_id`, `nama`, `address_1`, `address_2`, `telp`, `fax`, `image`, `kota`, `provinsi`, `email`) VALUES
(1, 12, 'Ayu Kayun', 'Jl. Kembang Jepun No.54-56, Surabaya 60162', 'Surabaya Pusat', '(031) 3521416 / 0819-7324-1373', '', '5820e-357x251.png', 'Surabaya', 'Jawa Timur', 'info@sumbertehnikjaya.com');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(15) NOT NULL,
  `title` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `image_big` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `image`, `image_big`, `sort`) VALUES
(1, 'Creative Communication Division', '9f4df-Banner Career_CC.jpg', 'f451f-PopUp_Banner CC.jpg', 1),
(4, 'Public Relation & Sales', '60b6c-Banner Career_PR.jpg', '60b6c-PopUp_Banner PR.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(15) NOT NULL,
  `province_id` int(11) NOT NULL,
  `province` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `postal_code` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `province_id`, `province`, `type`, `city_name`, `postal_code`) VALUES
(1, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat', '23600'),
(2, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Barat Daya', '23700'),
(3, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Besar', '23000'),
(4, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Jaya', '23600'),
(5, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Selatan', '23700'),
(6, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Singkil', '24700'),
(7, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tamiang', '24400'),
(8, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tengah', '24500'),
(9, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Tenggara', '24600'),
(10, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Timur', '24400'),
(11, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Aceh Utara', '24300'),
(12, 32, 'Sumatera Barat', 'Kabupaten', 'Agam', '26000'),
(13, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Alor', '85800'),
(14, 19, 'Maluku', 'Kota', 'Ambon', '97000'),
(15, 34, 'Sumatera Utara', 'Kabupaten', 'Asahan', '21000'),
(16, 24, 'Papua', 'Kabupaten', 'Asmat', '99700'),
(17, 1, 'Bali', 'Kabupaten', 'Badung', '80361'),
(18, 13, 'Kalimantan Selatan', 'Kabupaten', 'Balangan', '71400'),
(19, 15, 'Kalimantan Timur', 'Kota', 'Balikpapan', '76100'),
(20, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Banda Aceh', '23000'),
(21, 18, 'Lampung', 'Kota', 'Bandar Lampung', '35000'),
(22, 9, 'Jawa Barat', 'Kabupaten', 'Bandung', '40000'),
(23, 9, 'Jawa Barat', 'Kota', 'Bandung', '40000'),
(24, 9, 'Jawa Barat', 'Kabupaten', 'Bandung Barat', '40000'),
(25, 29, 'Sulawesi Tengah', 'Kabupaten', 'Banggai', '94791'),
(26, 29, 'Sulawesi Tengah', 'Kabupaten', 'Banggai Kepulauan', '94791'),
(27, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka', '33200'),
(28, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka Barat', '33300'),
(29, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka Selatan', '33700'),
(30, 2, 'Bangka Belitung', 'Kabupaten', 'Bangka Tengah', '33600'),
(31, 11, 'Jawa Timur', 'Kabupaten', 'Bangkalan', '69100'),
(32, 1, 'Bali', 'Kabupaten', 'Bangli', '80600'),
(33, 13, 'Kalimantan Selatan', 'Kabupaten', 'Banjar', '70600'),
(34, 9, 'Jawa Barat', 'Kota', 'Banjar', '46300'),
(35, 13, 'Kalimantan Selatan', 'Kota', 'Banjarbaru', '70700'),
(36, 13, 'Kalimantan Selatan', 'Kota', 'Banjarmasin', '70000'),
(37, 10, 'Jawa Tengah', 'Kabupaten', 'Banjarnegara', '53400'),
(38, 28, 'Sulawesi Selatan', 'Kabupaten', 'Bantaeng', '92400'),
(39, 5, 'DI Yogyakarta', 'Kabupaten', 'Bantul', '55700'),
(40, 33, 'Sumatera Selatan', 'Kabupaten', 'Banyuasin', '30758'),
(41, 10, 'Jawa Tengah', 'Kabupaten', 'Banyumas', '53100'),
(42, 11, 'Jawa Timur', 'Kabupaten', 'Banyuwangi', '68400'),
(43, 13, 'Kalimantan Selatan', 'Kabupaten', 'Barito Kuala', '70500'),
(44, 14, 'Kalimantan Tengah', 'Kabupaten', 'Barito Selatan', '73700'),
(45, 14, 'Kalimantan Tengah', 'Kabupaten', 'Barito Timur', '73600'),
(46, 14, 'Kalimantan Tengah', 'Kabupaten', 'Barito Utara', '73800'),
(47, 28, 'Sulawesi Selatan', 'Kabupaten', 'Barru', '90700'),
(48, 17, 'Kepulauan Riau', 'Kota', 'Batam', '29400'),
(49, 10, 'Jawa Tengah', 'Kabupaten', 'Batang', '51200'),
(50, 8, 'Jambi', 'Kabupaten', 'Batang Hari', '36600'),
(51, 11, 'Jawa Timur', 'Kota', 'Batu', '65311'),
(52, 34, 'Sumatera Utara', 'Kabupaten', 'Batu Bara', '21200'),
(53, 30, 'Sulawesi Tenggara', 'Kota', 'Bau-Bau', '93700'),
(54, 9, 'Jawa Barat', 'Kabupaten', 'Bekasi', '17000'),
(55, 9, 'Jawa Barat', 'Kota', 'Bekasi', '17000'),
(56, 2, 'Bangka Belitung', 'Kabupaten', 'Belitung', '33400'),
(57, 2, 'Bangka Belitung', 'Kabupaten', 'Belitung Timur', '33400'),
(58, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Belu', '85700'),
(59, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bener Meriah', '24500'),
(60, 26, 'Riau', 'Kabupaten', 'Bengkalis', '28700'),
(61, 12, 'Kalimantan Barat', 'Kabupaten', 'Bengkayang', '79200'),
(62, 4, 'Bengkulu', 'Kota', 'Bengkulu', '38000'),
(63, 4, 'Bengkulu', 'Kabupaten', 'Bengkulu Selatan', '38500'),
(64, 4, 'Bengkulu', 'Kabupaten', 'Bengkulu Tengah', '38000'),
(65, 4, 'Bengkulu', 'Kabupaten', 'Bengkulu Utara', '38600'),
(66, 15, 'Kalimantan Timur', 'Kabupaten', 'Berau', '77300'),
(67, 24, 'Papua', 'Kabupaten', 'Biak Numfor', '98100'),
(68, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Bima', '84100'),
(69, 22, 'Nusa Tenggara Barat (NTB)', 'Kota', 'Bima', '84100'),
(70, 34, 'Sumatera Utara', 'Kota', 'Binjai', '20700'),
(71, 17, 'Kepulauan Riau', 'Kabupaten', 'Bintan', '29100'),
(72, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Bireuen', '24200'),
(73, 31, 'Sulawesi Utara', 'Kota', 'Bitung', '95500'),
(74, 11, 'Jawa Timur', 'Kabupaten', 'Blitar', '66100'),
(75, 11, 'Jawa Timur', 'Kota', 'Blitar', '66100'),
(76, 10, 'Jawa Tengah', 'Kabupaten', 'Blora', '58200'),
(77, 7, 'Gorontalo', 'Kabupaten', 'Boalemo', '96200'),
(78, 9, 'Jawa Barat', 'Kabupaten', 'Bogor', '16000'),
(79, 9, 'Jawa Barat', 'Kota', 'Bogor', '16000'),
(80, 11, 'Jawa Timur', 'Kabupaten', 'Bojonegoro', '62100'),
(81, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow (Bolmong)', '95700'),
(82, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Selatan', '95700'),
(83, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Timur', '95700'),
(84, 31, 'Sulawesi Utara', 'Kabupaten', 'Bolaang Mongondow Utara', '95700'),
(85, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Bombana', '93700'),
(86, 11, 'Jawa Timur', 'Kabupaten', 'Bondowoso', '68200'),
(87, 28, 'Sulawesi Selatan', 'Kabupaten', 'Bone', '92552'),
(88, 7, 'Gorontalo', 'Kabupaten', 'Bone Bolango', '96184'),
(89, 15, 'Kalimantan Timur', 'Kota', 'Bontang', '75300'),
(90, 24, 'Papua', 'Kabupaten', 'Boven Digoel', '99600'),
(91, 10, 'Jawa Tengah', 'Kabupaten', 'Boyolali', '57300'),
(92, 10, 'Jawa Tengah', 'Kabupaten', 'Brebes', '52200'),
(93, 32, 'Sumatera Barat', 'Kota', 'Bukittinggi', '26100'),
(94, 1, 'Bali', 'Kabupaten', 'Buleleng', '81100'),
(95, 28, 'Sulawesi Selatan', 'Kabupaten', 'Bulukumba', '92500'),
(96, 16, 'Kalimantan Utara', 'Kabupaten', 'Bulungan (Bulongan)', '77200'),
(97, 8, 'Jambi', 'Kabupaten', 'Bungo', '37200'),
(98, 29, 'Sulawesi Tengah', 'Kabupaten', 'Buol', '94500'),
(99, 19, 'Maluku', 'Kabupaten', 'Buru', '97500'),
(100, 19, 'Maluku', 'Kabupaten', 'Buru Selatan', '97500'),
(101, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Buton', '93700'),
(102, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Buton Utara', '93600'),
(103, 9, 'Jawa Barat', 'Kabupaten', 'Ciamis', '46200'),
(104, 9, 'Jawa Barat', 'Kabupaten', 'Cianjur', '43200'),
(105, 10, 'Jawa Tengah', 'Kabupaten', 'Cilacap', '53200'),
(106, 3, 'Banten', 'Kota', 'Cilegon', '42400'),
(107, 9, 'Jawa Barat', 'Kota', 'Cimahi', '40500'),
(108, 9, 'Jawa Barat', 'Kabupaten', 'Cirebon', '45100'),
(109, 9, 'Jawa Barat', 'Kota', 'Cirebon', '45100'),
(110, 34, 'Sumatera Utara', 'Kabupaten', 'Dairi', '22200'),
(111, 24, 'Papua', 'Kabupaten', 'Deiyai (Deliyai)', '98700'),
(112, 34, 'Sumatera Utara', 'Kabupaten', 'Deli Serdang', '20500'),
(113, 10, 'Jawa Tengah', 'Kabupaten', 'Demak', '59500'),
(114, 1, 'Bali', 'Kota', 'Denpasar', '80000'),
(115, 9, 'Jawa Barat', 'Kota', 'Depok', '16400'),
(116, 32, 'Sumatera Barat', 'Kabupaten', 'Dharmasraya', '27600'),
(117, 24, 'Papua', 'Kabupaten', 'Dogiyai', '98800'),
(118, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Dompu', '84200'),
(119, 29, 'Sulawesi Tengah', 'Kabupaten', 'Donggala', '94351'),
(120, 26, 'Riau', 'Kota', 'Dumai', '28800'),
(121, 33, 'Sumatera Selatan', 'Kabupaten', 'Empat Lawang', '31500'),
(122, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ende', '86300'),
(123, 28, 'Sulawesi Selatan', 'Kabupaten', 'Enrekang', '91700'),
(124, 25, 'Papua Barat', 'Kabupaten', 'Fakfak', '98600'),
(125, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Flores Timur', '86200'),
(126, 9, 'Jawa Barat', 'Kabupaten', 'Garut', '44100'),
(127, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Gayo Lues', '24600'),
(128, 1, 'Bali', 'Kabupaten', 'Gianyar', '80500'),
(129, 7, 'Gorontalo', 'Kabupaten', 'Gorontalo', '96100'),
(130, 7, 'Gorontalo', 'Kota', 'Gorontalo', '96100'),
(131, 7, 'Gorontalo', 'Kabupaten', 'Gorontalo Utara', '96100'),
(132, 28, 'Sulawesi Selatan', 'Kabupaten', 'Gowa', '92100'),
(133, 11, 'Jawa Timur', 'Kabupaten', 'Gresik', '61100'),
(134, 10, 'Jawa Tengah', 'Kabupaten', 'Grobogan', '58152'),
(135, 5, 'DI Yogyakarta', 'Kabupaten', 'Gunung Kidul', '55800'),
(136, 14, 'Kalimantan Tengah', 'Kabupaten', 'Gunung Mas', '74500'),
(137, 34, 'Sumatera Utara', 'Kota', 'Gunungsitoli', '22800'),
(138, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Barat', '97700'),
(139, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Selatan', '97700'),
(140, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Tengah', '97800'),
(141, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Timur', '97800'),
(142, 20, 'Maluku Utara', 'Kabupaten', 'Halmahera Utara', '97700'),
(143, 13, 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Selatan', '71200'),
(144, 13, 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Tengah', '71300'),
(145, 13, 'Kalimantan Selatan', 'Kabupaten', 'Hulu Sungai Utara', '71400'),
(146, 34, 'Sumatera Utara', 'Kabupaten', 'Humbang Hasundutan', '22400'),
(147, 26, 'Riau', 'Kabupaten', 'Indragiri Hilir', '29200'),
(148, 26, 'Riau', 'Kabupaten', 'Indragiri Hulu', '29300'),
(149, 9, 'Jawa Barat', 'Kabupaten', 'Indramayu', '45200'),
(150, 24, 'Papua', 'Kabupaten', 'Intan Jaya', '98700'),
(151, 6, 'DKI Jakarta', 'Kota', 'Jakarta Barat', '11000'),
(152, 6, 'DKI Jakarta', 'Kota', 'Jakarta Pusat', '10000'),
(153, 6, 'DKI Jakarta', 'Kota', 'Jakarta Selatan', '12000'),
(154, 6, 'DKI Jakarta', 'Kota', 'Jakarta Timur', '13000'),
(155, 6, 'DKI Jakarta', 'Kota', 'Jakarta Utara', '14000'),
(156, 8, 'Jambi', 'Kota', 'Jambi', '36000'),
(157, 24, 'Papua', 'Kabupaten', 'Jayapura', '99000'),
(158, 24, 'Papua', 'Kota', 'Jayapura', '99000'),
(159, 24, 'Papua', 'Kabupaten', 'Jayawijaya', '99500'),
(160, 11, 'Jawa Timur', 'Kabupaten', 'Jember', '68100'),
(161, 1, 'Bali', 'Kabupaten', 'Jembrana', '82200'),
(162, 28, 'Sulawesi Selatan', 'Kabupaten', 'Jeneponto', '92300'),
(163, 10, 'Jawa Tengah', 'Kabupaten', 'Jepara', '59400'),
(164, 11, 'Jawa Timur', 'Kabupaten', 'Jombang', '61400'),
(165, 25, 'Papua Barat', 'Kabupaten', 'Kaimana', '98654'),
(166, 26, 'Riau', 'Kabupaten', 'Kampar', '28400'),
(167, 14, 'Kalimantan Tengah', 'Kabupaten', 'Kapuas', '73500'),
(168, 12, 'Kalimantan Barat', 'Kabupaten', 'Kapuas Hulu', '78700'),
(169, 10, 'Jawa Tengah', 'Kabupaten', 'Karanganyar', '57700'),
(170, 1, 'Bali', 'Kabupaten', 'Karangasem', '80800'),
(171, 9, 'Jawa Barat', 'Kabupaten', 'Karawang', '41300'),
(172, 17, 'Kepulauan Riau', 'Kabupaten', 'Karimun', '29600'),
(173, 34, 'Sumatera Utara', 'Kabupaten', 'Karo', '22100'),
(174, 14, 'Kalimantan Tengah', 'Kabupaten', 'Katingan', '74400'),
(175, 4, 'Bengkulu', 'Kabupaten', 'Kaur', '38000'),
(176, 12, 'Kalimantan Barat', 'Kabupaten', 'Kayong Utara', '78800'),
(177, 10, 'Jawa Tengah', 'Kabupaten', 'Kebumen', '54300'),
(178, 11, 'Jawa Timur', 'Kabupaten', 'Kediri', '64100'),
(179, 11, 'Jawa Timur', 'Kota', 'Kediri', '64100'),
(180, 24, 'Papua', 'Kabupaten', 'Keerom', '99000'),
(181, 10, 'Jawa Tengah', 'Kabupaten', 'Kendal', '51300'),
(182, 30, 'Sulawesi Tenggara', 'Kota', 'Kendari', '93000'),
(183, 4, 'Bengkulu', 'Kabupaten', 'Kepahiang', '39172'),
(184, 17, 'Kepulauan Riau', 'Kabupaten', 'Kepulauan Anambas', '29700'),
(185, 19, 'Maluku', 'Kabupaten', 'Kepulauan Aru', '97600'),
(186, 32, 'Sumatera Barat', 'Kabupaten', 'Kepulauan Mentawai', '25391'),
(187, 26, 'Riau', 'Kabupaten', 'Kepulauan Meranti', '28700'),
(188, 31, 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Sangihe', '95800'),
(189, 6, 'DKI Jakarta', 'Kabupaten', 'Kepulauan Seribu', '14530'),
(190, 31, 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '95800'),
(191, 20, 'Maluku Utara', 'Kabupaten', 'Kepulauan Sula', '97700'),
(192, 31, 'Sulawesi Utara', 'Kabupaten', 'Kepulauan Talaud', '95800'),
(193, 24, 'Papua', 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', '98200'),
(194, 8, 'Jambi', 'Kabupaten', 'Kerinci', '37100'),
(195, 12, 'Kalimantan Barat', 'Kabupaten', 'Ketapang', '78800'),
(196, 10, 'Jawa Tengah', 'Kabupaten', 'Klaten', '57400'),
(197, 1, 'Bali', 'Kabupaten', 'Klungkung', '80700'),
(198, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka', '93500'),
(199, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Kolaka Utara', '93500'),
(200, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Konawe', '93400'),
(201, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Selatan', '93000'),
(202, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Konawe Utara', '93000'),
(203, 13, 'Kalimantan Selatan', 'Kabupaten', 'Kotabaru', '72100'),
(204, 31, 'Sulawesi Utara', 'Kota', 'Kotamobagu', '95700'),
(205, 14, 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Barat', '74100'),
(206, 14, 'Kalimantan Tengah', 'Kabupaten', 'Kotawaringin Timur', '74300'),
(207, 26, 'Riau', 'Kabupaten', 'Kuantan Singingi', '29500'),
(208, 12, 'Kalimantan Barat', 'Kabupaten', 'Kubu Raya', '78000'),
(209, 10, 'Jawa Tengah', 'Kabupaten', 'Kudus', '59300'),
(210, 5, 'DI Yogyakarta', 'Kabupaten', 'Kulon Progo', '55600'),
(211, 9, 'Jawa Barat', 'Kabupaten', 'Kuningan', '45500'),
(212, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Kupang', '85000'),
(213, 23, 'Nusa Tenggara Timur (NTT)', 'Kota', 'Kupang', '85000'),
(214, 15, 'Kalimantan Timur', 'Kabupaten', 'Kutai Barat', '75000'),
(215, 15, 'Kalimantan Timur', 'Kabupaten', 'Kutai Kartanegara', '75500'),
(216, 15, 'Kalimantan Timur', 'Kabupaten', 'Kutai Timur', '75556'),
(217, 34, 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu', '21400'),
(218, 34, 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Selatan', '21400'),
(219, 34, 'Sumatera Utara', 'Kabupaten', 'Labuhan Batu Utara', '21400'),
(220, 33, 'Sumatera Selatan', 'Kabupaten', 'Lahat', '31400'),
(221, 14, 'Kalimantan Tengah', 'Kabupaten', 'Lamandau', '74100'),
(222, 11, 'Jawa Timur', 'Kabupaten', 'Lamongan', '62200'),
(223, 18, 'Lampung', 'Kabupaten', 'Lampung Barat', '35000'),
(224, 18, 'Lampung', 'Kabupaten', 'Lampung Selatan', '35000'),
(225, 18, 'Lampung', 'Kabupaten', 'Lampung Tengah', '34100'),
(226, 18, 'Lampung', 'Kabupaten', 'Lampung Timur', '34100'),
(227, 18, 'Lampung', 'Kabupaten', 'Lampung Utara', '34500'),
(228, 12, 'Kalimantan Barat', 'Kabupaten', 'Landak', '79357'),
(229, 34, 'Sumatera Utara', 'Kabupaten', 'Langkat', '20800'),
(230, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Langsa', '24400'),
(231, 24, 'Papua', 'Kabupaten', 'Lanny Jaya', '99500'),
(232, 3, 'Banten', 'Kabupaten', 'Lebak', '42300'),
(233, 4, 'Bengkulu', 'Kabupaten', 'Lebong', '39200'),
(234, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Lembata', '86600'),
(235, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Lhokseumawe', '24300'),
(236, 32, 'Sumatera Barat', 'Kabupaten', 'Lima Puluh Koto/Kota', '26200'),
(237, 17, 'Kepulauan Riau', 'Kabupaten', 'Lingga', '29800'),
(238, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Barat', '83363'),
(239, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Tengah', '83500'),
(240, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Timur', '83600'),
(241, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Lombok Utara', '83300'),
(242, 33, 'Sumatera Selatan', 'Kota', 'Lubuk Linggau', '31600'),
(243, 11, 'Jawa Timur', 'Kabupaten', 'Lumajang', '67300'),
(244, 28, 'Sulawesi Selatan', 'Kabupaten', 'Luwu', '91900'),
(245, 28, 'Sulawesi Selatan', 'Kabupaten', 'Luwu Timur', '91900'),
(246, 28, 'Sulawesi Selatan', 'Kabupaten', 'Luwu Utara', '91900'),
(247, 11, 'Jawa Timur', 'Kabupaten', 'Madiun', '63100'),
(248, 11, 'Jawa Timur', 'Kota', 'Madiun', '63100'),
(249, 10, 'Jawa Tengah', 'Kabupaten', 'Magelang', '56100'),
(250, 10, 'Jawa Tengah', 'Kota', 'Magelang', '56100'),
(251, 11, 'Jawa Timur', 'Kabupaten', 'Magetan', '63300'),
(252, 9, 'Jawa Barat', 'Kabupaten', 'Majalengka', '45400'),
(253, 27, 'Sulawesi Barat', 'Kabupaten', 'Majene', '91400'),
(254, 28, 'Sulawesi Selatan', 'Kota', 'Makassar', '90000'),
(255, 11, 'Jawa Timur', 'Kabupaten', 'Malang', '65100'),
(256, 11, 'Jawa Timur', 'Kota', 'Malang', '65100'),
(257, 16, 'Kalimantan Utara', 'Kabupaten', 'Malinau', '77154'),
(258, 19, 'Maluku', 'Kabupaten', 'Maluku Barat Daya', '97000'),
(259, 19, 'Maluku', 'Kabupaten', 'Maluku Tengah', '97500'),
(260, 19, 'Maluku', 'Kabupaten', 'Maluku Tenggara', '97600'),
(261, 19, 'Maluku', 'Kabupaten', 'Maluku Tenggara Barat', '97600'),
(262, 27, 'Sulawesi Barat', 'Kabupaten', 'Mamasa', '91363'),
(263, 24, 'Papua', 'Kabupaten', 'Mamberamo Raya', '99500'),
(264, 24, 'Papua', 'Kabupaten', 'Mamberamo Tengah', '99500'),
(265, 27, 'Sulawesi Barat', 'Kabupaten', 'Mamuju', '91500'),
(266, 27, 'Sulawesi Barat', 'Kabupaten', 'Mamuju Utara', '91500'),
(267, 31, 'Sulawesi Utara', 'Kota', 'Manado', '95000'),
(268, 34, 'Sumatera Utara', 'Kabupaten', 'Mandailing Natal', '22919'),
(269, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai', '86500'),
(270, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Barat', '86753'),
(271, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Manggarai Timur', '86500'),
(272, 25, 'Papua Barat', 'Kabupaten', 'Manokwari', '98300'),
(273, 25, 'Papua Barat', 'Kabupaten', 'Manokwari Selatan', '98300'),
(274, 24, 'Papua', 'Kabupaten', 'Mappi', '99000'),
(275, 28, 'Sulawesi Selatan', 'Kabupaten', 'Maros', '90500'),
(276, 22, 'Nusa Tenggara Barat (NTB)', 'Kota', 'Mataram', '83000'),
(277, 25, 'Papua Barat', 'Kabupaten', 'Maybrat', '99000'),
(278, 34, 'Sumatera Utara', 'Kota', 'Medan', '20000'),
(279, 12, 'Kalimantan Barat', 'Kabupaten', 'Melawi', '78672'),
(280, 8, 'Jambi', 'Kabupaten', 'Merangin', '37300'),
(281, 24, 'Papua', 'Kabupaten', 'Merauke', '99600'),
(282, 18, 'Lampung', 'Kabupaten', 'Mesuji', '34500'),
(283, 18, 'Lampung', 'Kota', 'Metro', '34100'),
(284, 24, 'Papua', 'Kabupaten', 'Mimika', '99900'),
(285, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa', '95600'),
(286, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa Selatan', '95000'),
(287, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa Tenggara', '95000'),
(288, 31, 'Sulawesi Utara', 'Kabupaten', 'Minahasa Utara', '95000'),
(289, 11, 'Jawa Timur', 'Kabupaten', 'Mojokerto', '61300'),
(290, 11, 'Jawa Timur', 'Kota', 'Mojokerto', '61300'),
(291, 29, 'Sulawesi Tengah', 'Kabupaten', 'Morowali', '94000'),
(292, 33, 'Sumatera Selatan', 'Kabupaten', 'Muara Enim', '31300'),
(293, 8, 'Jambi', 'Kabupaten', 'Muaro Jambi', '36365'),
(294, 4, 'Bengkulu', 'Kabupaten', 'Muko Muko', '38365'),
(295, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Muna', '93600'),
(296, 14, 'Kalimantan Tengah', 'Kabupaten', 'Murung Raya', '73900'),
(297, 33, 'Sumatera Selatan', 'Kabupaten', 'Musi Banyuasin', '30700'),
(298, 33, 'Sumatera Selatan', 'Kabupaten', 'Musi Rawas', '31600'),
(299, 24, 'Papua', 'Kabupaten', 'Nabire', '98800'),
(300, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Nagan Raya', '23600'),
(301, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Nagekeo', '86400'),
(302, 17, 'Kepulauan Riau', 'Kabupaten', 'Natuna', '29700'),
(303, 24, 'Papua', 'Kabupaten', 'Nduga', '99500'),
(304, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Ngada', '86400'),
(305, 11, 'Jawa Timur', 'Kabupaten', 'Nganjuk', '64400'),
(306, 11, 'Jawa Timur', 'Kabupaten', 'Ngawi', '63200'),
(307, 34, 'Sumatera Utara', 'Kabupaten', 'Nias', '22800'),
(308, 34, 'Sumatera Utara', 'Kabupaten', 'Nias Barat', '22800'),
(309, 34, 'Sumatera Utara', 'Kabupaten', 'Nias Selatan', '22800'),
(310, 34, 'Sumatera Utara', 'Kabupaten', 'Nias Utara', '22800'),
(311, 16, 'Kalimantan Utara', 'Kabupaten', 'Nunukan', '77182'),
(312, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Ilir', '30600'),
(313, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ilir', '30600'),
(314, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu', '32100'),
(315, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Selatan', '32100'),
(316, 33, 'Sumatera Selatan', 'Kabupaten', 'Ogan Komering Ulu Timur', '32100'),
(317, 11, 'Jawa Timur', 'Kabupaten', 'Pacitan', '63500'),
(318, 32, 'Sumatera Barat', 'Kota', 'Padang', '25000'),
(319, 34, 'Sumatera Utara', 'Kabupaten', 'Padang Lawas', '22700'),
(320, 34, 'Sumatera Utara', 'Kabupaten', 'Padang Lawas Utara', '22700'),
(321, 32, 'Sumatera Barat', 'Kota', 'Padang Panjang', '27100'),
(322, 32, 'Sumatera Barat', 'Kabupaten', 'Padang Pariaman', '25500'),
(323, 34, 'Sumatera Utara', 'Kota', 'Padang Sidempuan', '22700'),
(324, 33, 'Sumatera Selatan', 'Kota', 'Pagar Alam', '31500'),
(325, 34, 'Sumatera Utara', 'Kabupaten', 'Pakpak Bharat', '22200'),
(326, 14, 'Kalimantan Tengah', 'Kota', 'Palangka Raya', '73000'),
(327, 33, 'Sumatera Selatan', 'Kota', 'Palembang', '30000'),
(328, 28, 'Sulawesi Selatan', 'Kota', 'Palopo', '91900'),
(329, 29, 'Sulawesi Tengah', 'Kota', 'Palu', '94000'),
(330, 11, 'Jawa Timur', 'Kabupaten', 'Pamekasan', '69300'),
(331, 3, 'Banten', 'Kabupaten', 'Pandeglang', '42200'),
(332, 9, 'Jawa Barat', 'Kabupaten', 'Pangandaran', '46396'),
(333, 28, 'Sulawesi Selatan', 'Kabupaten', 'Pangkajene Kepulauan', '90600'),
(334, 2, 'Bangka Belitung', 'Kota', 'Pangkal Pinang', '33100'),
(335, 24, 'Papua', 'Kabupaten', 'Paniai', '98700'),
(336, 28, 'Sulawesi Selatan', 'Kota', 'Parepare', '91100'),
(337, 32, 'Sumatera Barat', 'Kota', 'Pariaman', '25500'),
(338, 29, 'Sulawesi Tengah', 'Kabupaten', 'Parigi Moutong', '94371'),
(339, 32, 'Sumatera Barat', 'Kabupaten', 'Pasaman', '26300'),
(340, 32, 'Sumatera Barat', 'Kabupaten', 'Pasaman Barat', '26300'),
(341, 15, 'Kalimantan Timur', 'Kabupaten', 'Paser', '76200'),
(342, 11, 'Jawa Timur', 'Kabupaten', 'Pasuruan', '67100'),
(343, 11, 'Jawa Timur', 'Kota', 'Pasuruan', '67100'),
(344, 10, 'Jawa Tengah', 'Kabupaten', 'Pati', '59100'),
(345, 32, 'Sumatera Barat', 'Kota', 'Payakumbuh', '26200'),
(346, 25, 'Papua Barat', 'Kabupaten', 'Pegunungan Arfak', '98300'),
(347, 24, 'Papua', 'Kabupaten', 'Pegunungan Bintang', '99500'),
(348, 10, 'Jawa Tengah', 'Kabupaten', 'Pekalongan', '51100'),
(349, 10, 'Jawa Tengah', 'Kota', 'Pekalongan', '51100'),
(350, 26, 'Riau', 'Kota', 'Pekanbaru', '28000'),
(351, 26, 'Riau', 'Kabupaten', 'Pelalawan', '28300'),
(352, 10, 'Jawa Tengah', 'Kabupaten', 'Pemalang', '52300'),
(353, 34, 'Sumatera Utara', 'Kota', 'Pematang Siantar', '21100'),
(354, 15, 'Kalimantan Timur', 'Kabupaten', 'Penajam Paser Utara', '76141'),
(355, 18, 'Lampung', 'Kabupaten', 'Pesawaran', '35000'),
(356, 18, 'Lampung', 'Kabupaten', 'Pesisir Barat', '34574'),
(357, 32, 'Sumatera Barat', 'Kabupaten', 'Pesisir Selatan', '25600'),
(358, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie', '24100'),
(359, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Pidie Jaya', '24100'),
(360, 28, 'Sulawesi Selatan', 'Kabupaten', 'Pinrang', '91200'),
(361, 7, 'Gorontalo', 'Kabupaten', 'Pohuwato', '96200'),
(362, 27, 'Sulawesi Barat', 'Kabupaten', 'Polewali Mandar', '91300'),
(363, 11, 'Jawa Timur', 'Kabupaten', 'Ponorogo', '63400'),
(364, 12, 'Kalimantan Barat', 'Kabupaten', 'Pontianak', '78000'),
(365, 12, 'Kalimantan Barat', 'Kota', 'Pontianak', '78000'),
(366, 29, 'Sulawesi Tengah', 'Kabupaten', 'Poso', '94600'),
(367, 33, 'Sumatera Selatan', 'Kota', 'Prabumulih', '31100'),
(368, 18, 'Lampung', 'Kabupaten', 'Pringsewu', '35373'),
(369, 11, 'Jawa Timur', 'Kabupaten', 'Probolinggo', '67200'),
(370, 11, 'Jawa Timur', 'Kota', 'Probolinggo', '67200'),
(371, 14, 'Kalimantan Tengah', 'Kabupaten', 'Pulang Pisau', '73561'),
(372, 20, 'Maluku Utara', 'Kabupaten', 'Pulau Morotai', '97771'),
(373, 24, 'Papua', 'Kabupaten', 'Puncak', '98900'),
(374, 24, 'Papua', 'Kabupaten', 'Puncak Jaya', '98900'),
(375, 10, 'Jawa Tengah', 'Kabupaten', 'Purbalingga', '53300'),
(376, 9, 'Jawa Barat', 'Kabupaten', 'Purwakarta', '41100'),
(377, 10, 'Jawa Tengah', 'Kabupaten', 'Purworejo', '54100'),
(378, 25, 'Papua Barat', 'Kabupaten', 'Raja Ampat', '98400'),
(379, 4, 'Bengkulu', 'Kabupaten', 'Rejang Lebong', '39100'),
(380, 10, 'Jawa Tengah', 'Kabupaten', 'Rembang', '59200'),
(381, 26, 'Riau', 'Kabupaten', 'Rokan Hilir', '28991'),
(382, 26, 'Riau', 'Kabupaten', 'Rokan Hulu', '28455'),
(383, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Rote Ndao', '85974'),
(384, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Sabang', '23500'),
(385, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sabu Raijua', '85391'),
(386, 10, 'Jawa Tengah', 'Kota', 'Salatiga', '50700'),
(387, 15, 'Kalimantan Timur', 'Kota', 'Samarinda', '75000'),
(388, 12, 'Kalimantan Barat', 'Kabupaten', 'Sambas', '79400'),
(389, 34, 'Sumatera Utara', 'Kabupaten', 'Samosir', '22300'),
(390, 11, 'Jawa Timur', 'Kabupaten', 'Sampang', '69200'),
(391, 12, 'Kalimantan Barat', 'Kabupaten', 'Sanggau', '78500'),
(392, 24, 'Papua', 'Kabupaten', 'Sarmi', '99373'),
(393, 8, 'Jambi', 'Kabupaten', 'Sarolangun', '37300'),
(394, 32, 'Sumatera Barat', 'Kota', 'Sawah Lunto', '27400'),
(395, 12, 'Kalimantan Barat', 'Kabupaten', 'Sekadau', '78582'),
(396, 28, 'Sulawesi Selatan', 'Kabupaten', 'Selayar (Kepulauan Selayar)', '92800'),
(397, 4, 'Bengkulu', 'Kabupaten', 'Seluma', '38000'),
(398, 10, 'Jawa Tengah', 'Kabupaten', 'Semarang', '50000'),
(399, 10, 'Jawa Tengah', 'Kota', 'Semarang', '50000'),
(400, 19, 'Maluku', 'Kabupaten', 'Seram Bagian Barat', '97500'),
(401, 19, 'Maluku', 'Kabupaten', 'Seram Bagian Timur', '97500'),
(402, 3, 'Banten', 'Kabupaten', 'Serang', '42100'),
(403, 3, 'Banten', 'Kota', 'Serang', '42100'),
(404, 34, 'Sumatera Utara', 'Kabupaten', 'Serdang Bedagai', '20000'),
(405, 14, 'Kalimantan Tengah', 'Kabupaten', 'Seruyan', '74200'),
(406, 26, 'Riau', 'Kabupaten', 'Siak', '28686'),
(407, 34, 'Sumatera Utara', 'Kota', 'Sibolga', '22500'),
(408, 28, 'Sulawesi Selatan', 'Kabupaten', 'Sidenreng Rappang/Rapang', '91600'),
(409, 11, 'Jawa Timur', 'Kabupaten', 'Sidoarjo', '61200'),
(410, 29, 'Sulawesi Tengah', 'Kabupaten', 'Sigi', '94000'),
(411, 32, 'Sumatera Barat', 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', '27500'),
(412, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sikka', '86100'),
(413, 34, 'Sumatera Utara', 'Kabupaten', 'Simalungun', '21100'),
(414, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kabupaten', 'Simeulue', '23000'),
(415, 12, 'Kalimantan Barat', 'Kota', 'Singkawang', '79100'),
(416, 28, 'Sulawesi Selatan', 'Kabupaten', 'Sinjai', '92600'),
(417, 12, 'Kalimantan Barat', 'Kabupaten', 'Sintang', '78600'),
(418, 11, 'Jawa Timur', 'Kabupaten', 'Situbondo', '68300'),
(419, 5, 'DI Yogyakarta', 'Kabupaten', 'Sleman', '55500'),
(420, 32, 'Sumatera Barat', 'Kabupaten', 'Solok', '27300'),
(421, 32, 'Sumatera Barat', 'Kota', 'Solok', '27300'),
(422, 32, 'Sumatera Barat', 'Kabupaten', 'Solok Selatan', '27300'),
(423, 28, 'Sulawesi Selatan', 'Kabupaten', 'Soppeng', '90800'),
(424, 25, 'Papua Barat', 'Kabupaten', 'Sorong', '98400'),
(425, 25, 'Papua Barat', 'Kota', 'Sorong', '98400'),
(426, 25, 'Papua Barat', 'Kabupaten', 'Sorong Selatan', '98400'),
(427, 10, 'Jawa Tengah', 'Kabupaten', 'Sragen', '57200'),
(428, 9, 'Jawa Barat', 'Kabupaten', 'Subang', '41200'),
(429, 21, 'Nanggroe Aceh Darussalam (NAD)', 'Kota', 'Subulussalam', '23782'),
(430, 9, 'Jawa Barat', 'Kabupaten', 'Sukabumi', '43100'),
(431, 9, 'Jawa Barat', 'Kota', 'Sukabumi', '43100'),
(432, 14, 'Kalimantan Tengah', 'Kabupaten', 'Sukamara', '74172'),
(433, 10, 'Jawa Tengah', 'Kabupaten', 'Sukoharjo', '57500'),
(434, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat', '87200'),
(435, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Barat Daya', '87200'),
(436, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Tengah', '87200'),
(437, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Sumba Timur', '87100'),
(438, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa', '84300'),
(439, 22, 'Nusa Tenggara Barat (NTB)', 'Kabupaten', 'Sumbawa Barat', '84300'),
(440, 9, 'Jawa Barat', 'Kabupaten', 'Sumedang', '45300'),
(441, 11, 'Jawa Timur', 'Kabupaten', 'Sumenep', '69400'),
(442, 8, 'Jambi', 'Kota', 'Sungaipenuh', '37100'),
(443, 24, 'Papua', 'Kabupaten', 'Supiori', '98100'),
(444, 11, 'Jawa Timur', 'Kota', 'Surabaya', '60000'),
(445, 10, 'Jawa Tengah', 'Kota', 'Surakarta (Solo)', '57100'),
(446, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tabalong', '71500'),
(447, 1, 'Bali', 'Kabupaten', 'Tabanan', '82100'),
(448, 28, 'Sulawesi Selatan', 'Kabupaten', 'Takalar', '92200'),
(449, 25, 'Papua Barat', 'Kabupaten', 'Tambrauw', '98400'),
(450, 16, 'Kalimantan Utara', 'Kabupaten', 'Tana Tidung', '77152'),
(451, 28, 'Sulawesi Selatan', 'Kabupaten', 'Tana Toraja', '91800'),
(452, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tanah Bumbu', '70000'),
(453, 32, 'Sumatera Barat', 'Kabupaten', 'Tanah Datar', '27200'),
(454, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tanah Laut', '70800'),
(455, 3, 'Banten', 'Kabupaten', 'Tangerang', '15000'),
(456, 3, 'Banten', 'Kota', 'Tangerang', '15000'),
(457, 3, 'Banten', 'Kota', 'Tangerang Selatan', '15000'),
(458, 18, 'Lampung', 'Kabupaten', 'Tanggamus', '35000'),
(459, 34, 'Sumatera Utara', 'Kota', 'Tanjung Balai', '21300'),
(460, 8, 'Jambi', 'Kabupaten', 'Tanjung Jabung Barat', '36500'),
(461, 8, 'Jambi', 'Kabupaten', 'Tanjung Jabung Timur', '36500'),
(462, 17, 'Kepulauan Riau', 'Kota', 'Tanjung Pinang', '29100'),
(463, 34, 'Sumatera Utara', 'Kabupaten', 'Tapanuli Selatan', '22700'),
(464, 34, 'Sumatera Utara', 'Kabupaten', 'Tapanuli Tengah', '22500'),
(465, 34, 'Sumatera Utara', 'Kabupaten', 'Tapanuli Utara', '22400'),
(466, 13, 'Kalimantan Selatan', 'Kabupaten', 'Tapin', '71100'),
(467, 16, 'Kalimantan Utara', 'Kota', 'Tarakan', '77100'),
(468, 9, 'Jawa Barat', 'Kabupaten', 'Tasikmalaya', '46100'),
(469, 9, 'Jawa Barat', 'Kota', 'Tasikmalaya', '46100'),
(470, 34, 'Sumatera Utara', 'Kota', 'Tebing Tinggi', '20600'),
(471, 8, 'Jambi', 'Kabupaten', 'Tebo', '37200'),
(472, 10, 'Jawa Tengah', 'Kabupaten', 'Tegal', '52100'),
(473, 10, 'Jawa Tengah', 'Kota', 'Tegal', '52100'),
(474, 25, 'Papua Barat', 'Kabupaten', 'Teluk Bintuni', '98300'),
(475, 25, 'Papua Barat', 'Kabupaten', 'Teluk Wondama', '98300'),
(476, 10, 'Jawa Tengah', 'Kabupaten', 'Temanggung', '56200'),
(477, 20, 'Maluku Utara', 'Kota', 'Ternate', '97700'),
(478, 20, 'Maluku Utara', 'Kota', 'Tidore Kepulauan', '97800'),
(479, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Selatan', '85500'),
(480, 23, 'Nusa Tenggara Timur (NTT)', 'Kabupaten', 'Timor Tengah Utara', '85600'),
(481, 34, 'Sumatera Utara', 'Kabupaten', 'Toba Samosir', '22300'),
(482, 29, 'Sulawesi Tengah', 'Kabupaten', 'Tojo Una-Una', '94600'),
(483, 29, 'Sulawesi Tengah', 'Kabupaten', 'Toli-Toli', '94500'),
(484, 24, 'Papua', 'Kabupaten', 'Tolikara', '99562'),
(485, 31, 'Sulawesi Utara', 'Kota', 'Tomohon', '95362'),
(486, 28, 'Sulawesi Selatan', 'Kabupaten', 'Toraja Utara', '91890'),
(487, 11, 'Jawa Timur', 'Kabupaten', 'Trenggalek', '66300'),
(488, 19, 'Maluku', 'Kota', 'Tual', '97600'),
(489, 11, 'Jawa Timur', 'Kabupaten', 'Tuban', '62300'),
(490, 18, 'Lampung', 'Kabupaten', 'Tulang Bawang', '34500'),
(491, 18, 'Lampung', 'Kabupaten', 'Tulang Bawang Barat', '34500'),
(492, 11, 'Jawa Timur', 'Kabupaten', 'Tulungagung', '66200'),
(493, 28, 'Sulawesi Selatan', 'Kabupaten', 'Wajo', '90900'),
(494, 30, 'Sulawesi Tenggara', 'Kabupaten', 'Wakatobi', '93700'),
(495, 24, 'Papua', 'Kabupaten', 'Waropen', '98200'),
(496, 18, 'Lampung', 'Kabupaten', 'Way Kanan', '35000'),
(497, 10, 'Jawa Tengah', 'Kabupaten', 'Wonogiri', '57600'),
(498, 10, 'Jawa Tengah', 'Kabupaten', 'Wonosobo', '56300'),
(499, 24, 'Papua', 'Kabupaten', 'Yahukimo', '99500'),
(500, 24, 'Papua', 'Kabupaten', 'Yalimo', '99500'),
(501, 5, 'DI Yogyakarta', 'Kota', 'Yogyakarta', '55000');

-- --------------------------------------------------------

--
-- Table structure for table `cs_customer`
--

CREATE TABLE `cs_customer` (
  `id` int(15) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `group_member_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `date_join` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cs_customer_address`
--

CREATE TABLE `cs_customer_address` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal_code` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `factory`
--

CREATE TABLE `factory` (
  `id` int(15) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `peta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `factory`
--

INSERT INTO `factory` (`id`, `nama`, `alamat`, `telp`, `email`, `peta`) VALUES
(3, 'FACTORY BOJONEGORO', 'Jl. Mahakam 225\r\nBojonegoro 20456\r\nEast Java', '+62 31 5015588', 'factory@realfood.id', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.8424071457857!2d111.89208239273015!3d-7.144214893503072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7782210551c057%3A0x247ac1865eaf2a1a!2sJl.+Jaksa+Agung+Suprapto%2C+Kec.+Bojonegoro%2C+Bojonegoro%2C+Jawa+Timur!5e0!3m2!1sid!2sid!4v1449301118786'),
(4, 'OFFICE SURABAYA', 'Jl. Ngagel Jaya 12\r\nSurabaya 60456\r\nEast Java', '+62 31 5015588', 'office@realfood.id', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.8424071457857!2d111.89208239273015!3d-7.144214893503072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7782210551c057%3A0x247ac1865eaf2a1a!2sJl.+Jaksa+Agung+Suprapto%2C+Kec.+Bojonegoro%2C+Bojonegoro%2C+Jawa+Timur!5e0!3m2!1sid!2sid!4v1449301118786'),
(5, 'OFFICE JAKARTA', 'Jl. Mahakam 225\r\nSidoarjo 20456\r\nEast Java', '+62 31 5015588', 'realfood@realfood.id', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1978.075813030622!2d112.71914446656403!3d-7.448471635049679!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7e6b5e9bccdbd%3A0xfb1776699d75a67a!2sJl.+Jaksa+Agung+Suprapto%2C+Kec.+Sidoarjo%2C+Sidoarjo%2C+Jawa+Timur!5e0!3m2!1sid!2sid!4v1450785636022');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(15) NOT NULL,
  `question` varchar(200) NOT NULL,
  `answer` text NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `sort`) VALUES
(4, 'Apa manfaat utama produk Xado Revitalizant?', 'Xado Revitalizant dapat memberikan perlindungan pada mesin agar terjaga dari keausan sehingga mempermudah anda dalam merawat mesin', 1),
(5, 'Apa yang membedakan produk Xado dengan produk – produk additive yang lain?', 'Xado bukanlah additive, namun merupakan produk revitalizant. Additive memperbaiki kulitas oli sedangkan Xado memperbaiki kualiatas mesin. Additive bercampur dengan oli sedangkan Xado hanya menggunakan oli sebagai media untuk bereaksi dengan lapisan mesin yang mengalami keausan', 2),
(6, 'Apakah produk Xado Revitalizant dapat digunakan untuk merek oli apa saja?', 'Bisa, karena Xado tidak bereaksi dengan oli sehingga tidak berpengaruh jika menggunakan merek oli apa saja selama oli yang digunakan masih baru', 3),
(7, 'Produk Xado dapat digunakan untuk mesin apa saja?', 'Xado dapat digunakan untuk mesin apa saja selama di dalam mesin tersebut terdapat komponen dua logam yang bergesekan dengan temperature yang tinggi serta menggunakan pelumasan. Misalnya : Mesin kendaraan bermotor, Genset, Kompresor, Hidrolis, Gearbox, Alat pemotong rumput dan lain – lain', 4),
(8, 'Apakah  setiap ganti oli saya harus memasukkan produk Xado kembali', 'Tidak perlu, karena produk Xado akan tetap melekat dalam permukaan mesin selama 100.000 km jika  setelah memasukkan produk Xado penggantian oli dilakukan setelah 4.000 km. Pada kilometer tersebut proses revitalisasi sudah sempurna sehingga produk Xado tidak akan ikut terbuang meskipun dilakukan pergantian oli yang berulang – ulang', 5),
(9, 'Apa yang membuat produk Xado tetap melekat pada permukaan mesin meskipun oli di buang?', 'Reviltalizant bekerja dengan prinsip nanopartikel. Pada saat temperatur dan tekanan tinggi produk Xado bereaksi dengan logam permukaan mesin membentuk karbida logam yang meresap ke permukaan logam dengan ukuran yang sangat kecil (nanometer)', 6),
(10, 'Apakah produk Xado Revitalizant bisa digunakan untuk bahan bakar solar?', 'Bisa, Xado memiliki berbagai macam produk yang digunakan untuk bahan bakar bensin, solar dan gas (BBG)', 7),
(11, 'Apakah produk Xado bisa menghemat bahan bakar?', 'Prinsip utama produk Xado adalah membentuk lapisan baru pada permukaan mesin sehingga dengan usaha yang sama dengan sebelumnya (putaran) mesin bekerja dengan tenaga yang lebih, maka terjadilah penghematan bahan bakar.', 8),
(12, 'Apa yang membedakan antara Xado One Stage dengan AMC New Car atau AMC Maximum?', 'Pada dasarnya Xado One Stage dan AMC memiliki kandungan Revitlaizant yang sama, yang menjadi perbedaan adalah dalam AMC terdapat dua zat lain yaitu metal conditioner dan sliding agent yang membuat proses revitalisasi jadi lebih cepat. Sedangkan untuk perbedaan AMC new Car dengan Maximum adalah jika New car digunakan untuk mesin mobil yang odometernya dibawah 20.000 km sedangkan Maximum digunakan untuk mobil dengan odometer lebih dari 20.000 km', 9),
(13, 'Odometer mobil saya adalah 150.000 km, apakah masih bisa menggunakan produk Xado Revitalizant?', 'Bisa, namun karena odometer telah mencapai 150.000 yang dapat digolongkan mesin yang lama maka sebelum dimasukkan produk AMC Maximum, sistem pelumasannya harus di bersihkan dengan produk Vitaflush demi menjamin keefektifan Revitalizant', 10),
(14, 'JIka saya sudah mengganti oli dan sudah berjalan 600 km apakah masih bisa dimasukkan produk Xado tanpa mengganti oli baru?', 'Bisa, selama odometer belum mencapai 1000 km maka bisa dimasukkan produk Xado Revitalizant tanpa mengganti oli', 11),
(15, 'Jika kendaran saya mengalami kebocoran oli apakah bisa bisa ditanggulangi oleh produk Xado?', 'Bisa, selama kebocoran oli tersebut tidak lebih dari 15% selama jarak tempuh 1000 km dan kebocoran tidak terjadi pada gasket atau seal – sealnya', 12),
(16, 'Jika asap putih sudah keluar dari saluran pembuangan apakah masih bisa ditanggulangi dengan produk Xado?', 'Biasanya jika sudah mengeluarkan asap berwarna putih pekat maka kebocoran telah lebih dari 15% maka produk Xado tidak dapat menganggulangi kebocoran mesin', 13),
(17, 'Mesin mobil saya mengalami “kepincangan”, apakah masih bisa digunakan produk Xado?', 'Produk Xado hanya dapat mengatasi problematika mesin yang sederhana, jika mesin sudah mengalami kepincangan disarankan untuk memeperbaiki sistem di dalam silinder terlebih dahulu, lalu setelah itu di masukkan produk Xado untuk menjaga mesin agar tidak terjadi kepincangan kembali', 14),
(18, 'Apakah petroltank dapat merubah premium yang kita pakai menjadi pertamax?', 'Prinsip utama petroltank adalah membersihkan zat pengotor pada bahan bakar dan saluran  bahan bakar sehingga dapat memperbaiki kualitas premium namun tidak untuk menaikkan nilai oktan. Jadi premium menjadi lebih bersih namun tidak berubah jadi pertamax', 15),
(19, 'Apakah ada produk Xado untuk menaikkan nilai oktan dan nilai cetan?', 'Ada, Octane Booster dapat menaikkan nilai oktan hingga 6 poin, sedangkan Cetane Booster dapat menaikkan nilai cetane hingga 7 poin', 16),
(20, 'Apakah terdapat produk Xado untuk sepeda motor dua tak? Dan bagaimana cara penggunaannya?', 'Produk Xado untuk motor adalah Small Engine, dapat digunakan baik untuk 4 tak ataupun dua tak. Cara penggunaannya adalah untuk 4 tak produk xado di masukkan pada tabung pengisian oli sedangkan untuk 2 tak produk Xado dimasukkan ke dalam tabung bahan bakar karena dalam 2 tak oli bercampur dengan bahan bakar', 17),
(21, 'Apakah perbedaan oli Xado dengan merek – merek oli yang lain?', 'Di dalam produk xado terdapat revitalizant, sehingga oli Xado tidak hanya berfungsi sebagai pelumas tapi juga berfungsi sebagai pelindung dari keausan. Oli Xado juga memiliki umur pakai yang lebih lama dibandingkan dengan oli yang lain', 18),
(22, 'Apakah saya hanya memasukkan produk Xado Revitalizant tanpa perlu menggunakan oli khusus Xado?', 'Karena pada dasarnya Xado Revitalizant dapat digunakan dapat digunakan untuk semua merek oli maka Xado revitalizant bisa digunakan tanpa perlu menggunakan oli khusus Xado, namun agar perlindungan kendaraan anda lebih maksimal maka kami merekomendasikan untuk menggunakan oli Xado juga', 19),
(23, 'Apakah Xado dapat di gunakan untuk mesin – mesin industri?', 'Kami memiliki produk untuk mesin – mesin industry seperti XGR for Compresor, XGR 3 Stage dan XGR 1 stage untuk genset dan alat – alat berat serta XGR for Hidrolics untuk mesin – mesin hidrolis', 20);

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery`
--

CREATE TABLE `gal_gallery` (
  `id` int(15) NOT NULL,
  `topik_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `writer` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `orientation` int(11) NOT NULL,
  `color` varchar(100) NOT NULL,
  `image2` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery_description`
--

CREATE TABLE `gal_gallery_description` (
  `id` int(15) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` text NOT NULL,
  `sub_title_2` text NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gal_gallery_image`
--

CREATE TABLE `gal_gallery_image` (
  `id` int(15) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `sort`, `status`) VALUES
(2, 'English', 'en', 1, '1'),
(3, 'Indonesia', 'id', 2, '0');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `username`, `activity`, `time`) VALUES
(1, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(2, 'info@markdesign.net', 'PrdCategoryController Create 1', '0000-00-00 00:00:00'),
(3, 'info@markdesign.net', 'PrdCategoryController Create 2', '0000-00-00 00:00:00'),
(4, 'info@markdesign.net', 'PrdProduct Controller Create 1', '0000-00-00 00:00:00'),
(5, 'info@markdesign.net', 'PrdCategoryController Create 3', '0000-00-00 00:00:00'),
(6, 'info@markdesign.net', 'PrdCategoryController Create 4', '0000-00-00 00:00:00'),
(7, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(8, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(9, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(10, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(11, 'info@markdesign.net', 'SlideController Update 1', '0000-00-00 00:00:00'),
(12, 'info@markdesign.net', 'SlideController Update 1', '0000-00-00 00:00:00'),
(13, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(14, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(15, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(16, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(17, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(18, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(19, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(20, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(21, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(22, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(23, 'info@markdesign.net', 'Blog Controller Create 1', '0000-00-00 00:00:00'),
(24, 'info@markdesign.net', 'Blog Controller Create 2', '0000-00-00 00:00:00'),
(25, 'info@markdesign.net', 'Blog Controller Create 1', '0000-00-00 00:00:00'),
(26, 'info@markdesign.net', 'Blog Controller Create 2', '0000-00-00 00:00:00'),
(27, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(28, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(29, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(30, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(31, 'info@markdesign.net', 'ProductController Update 1', '0000-00-00 00:00:00'),
(32, 'deoryzpandu@gmail.com', 'Login: deoryzpandu@gmail.com', '2020-07-03 04:56:21'),
(33, 'deoryzpandu@gmail.com', 'Blog Controller Create 1', '2020-07-03 04:59:51'),
(34, 'deoryzpandu@gmail.com', 'Blog Controller Create 2', '2020-07-03 05:00:04'),
(35, 'deoryzpandu@gmail.com', 'Blog Controller Create 3', '2020-07-03 05:00:16'),
(36, 'deoryzpandu@gmail.com', 'BlogController Update 3', '2020-07-03 05:00:25'),
(37, 'deoryzpandu@gmail.com', 'BlogController Update 3', '2020-07-03 05:00:47'),
(38, 'deoryzpandu@gmail.com', 'BlogController Update 2', '2020-07-03 05:01:16'),
(39, 'deoryzpandu@gmail.com', 'BlogController Update 1', '2020-07-03 05:01:43'),
(40, 'deoryzpandu@gmail.com', 'Setting Update', '2020-07-03 05:13:35'),
(41, 'deoryzpandu@gmail.com', 'Setting Update', '2020-07-03 05:15:31'),
(42, 'deoryzpandu@gmail.com', 'Setting Update', '2020-07-03 05:21:01'),
(43, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(44, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(45, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(46, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(47, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(48, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(49, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(50, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(51, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(52, 'info@markdesign.net', 'Setting Update', '0000-00-00 00:00:00'),
(53, 'info@markdesign.net', 'Slide Controller Create 2', '0000-00-00 00:00:00'),
(54, 'info@markdesign.net', 'Slide Controller Create 3', '0000-00-00 00:00:00'),
(55, 'info@markdesign.net', 'Slide Controller Create 4', '0000-00-00 00:00:00'),
(56, 'info@markdesign.net', 'Slide Controller Create 5', '0000-00-00 00:00:00'),
(57, 'info@markdesign.net', 'SlideController Update 5', '0000-00-00 00:00:00'),
(58, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(59, 'info@markdesign.net', 'BlogController Update 3', '0000-00-00 00:00:00'),
(60, 'info@markdesign.net', 'BlogController Update 2', '0000-00-00 00:00:00'),
(61, 'info@markdesign.net', 'BlogController Update 1', '0000-00-00 00:00:00'),
(62, 'deoryzpandu@gmail.com', 'Login: deoryzpandu@gmail.com', '0000-00-00 00:00:00'),
(63, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(64, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(65, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(66, 'info@markdesign.net', 'Login: info@markdesign.net', '0000-00-00 00:00:00'),
(67, 'deoryzpandu@gmail.com', 'Login: deoryzpandu@gmail.com', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `me_member`
--

CREATE TABLE `me_member` (
  `id` int(15) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `me_member`
--

INSERT INTO `me_member` (`id`, `email`, `first_name`, `last_name`, `pass`, `login_terakhir`, `aktivasi`, `aktif`, `image`, `hp`, `address`, `city`, `province`, `postcode`) VALUES
(2, 'deoryzpandu@gmail.com', 'deory pandu putra', 'wahyu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2015-07-10 16:31:10', 0, 1, '', '0854646464', 'jl test test', 'batu', '11', '65656'),
(5, 'ibnu@markdesign.net', 'deory pandu putra', 'wahyu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-03-28 04:16:03', 0, 1, '', '085646765265', 'Jl Martorejo No 113', 'Batu', '11', '65323'),
(9, 'ibnu.fajar86@yahoo.com', 'ibnu', 'fajar', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-03-29 09:11:18', 0, 1, '', '2736473264', 'jl. embong kenongo 79a', 'surabaya', '11', '60239'),
(7, 'deo@markdesign.net', 'deoryzzz', 'pandu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-03-29 08:59:20', 0, 1, '', '085646765265', 'Jl Martorejo No 113', '444', '11', '65323'),
(10, 'deoryz@yahoo.co.id', 'deoryzzz', 'pandu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2016-03-29 09:12:15', 0, 1, '', '085646765265', 'Jl Martorejo No 113', 'batu', '11', '65323'),
(11, 'chendra@markdesign.net', 'chendra', 'cahyadi', '340ea6163337bd031bb14fe1885c1c92cb78766a', '2016-04-13 07:59:22', 0, 1, '', '03160251101', 'bawean 50', 'surabaya', '11', '60246'),
(12, 'dindacholifahputri@gmail.com', 'Dinda', 'Cholifah Putri', '5f0ad6d66d4eac4172fee264ee88ccc2b77c3840', '2016-06-16 06:34:54', 0, 1, '', '089675783665', 'Jl.Brigjen Katamso gg.Anggrek 2 No.27', 'Sidoarjo', '11', '61256');

-- --------------------------------------------------------

--
-- Table structure for table `or_order`
--

CREATE TABLE `or_order` (
  `id` int(15) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_prefix` varchar(20) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(128) NOT NULL,
  `payment_first_name` varchar(128) NOT NULL,
  `payment_last_name` varchar(128) NOT NULL,
  `payment_company` varchar(128) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(128) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `shipping_first_name` varchar(128) NOT NULL,
  `shipping_last_name` varchar(128) NOT NULL,
  `shipping_company` varchar(128) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(128) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_area` int(11) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(100) NOT NULL,
  `currency_value` decimal(15,4) NOT NULL,
  `ip` varchar(128) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_modif` datetime NOT NULL,
  `delivery_from` varchar(100) NOT NULL,
  `delivery_to` varchar(100) NOT NULL,
  `delivery_package` varchar(100) NOT NULL,
  `delivery_price` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `delivery_weight` int(11) NOT NULL,
  `token` varchar(200) NOT NULL,
  `tracking_id` varchar(200) NOT NULL,
  `is_read` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order`
--

INSERT INTO `or_order` (`id`, `invoice_no`, `invoice_prefix`, `customer_id`, `customer_group_id`, `first_name`, `last_name`, `email`, `phone`, `payment_first_name`, `payment_last_name`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_zone`, `payment_country`, `shipping_first_name`, `shipping_last_name`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_zone`, `shipping_area`, `shipping_country`, `comment`, `tax`, `total`, `order_status_id`, `affiliate_id`, `commission`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `date_add`, `date_modif`, `delivery_from`, `delivery_to`, `delivery_package`, `delivery_price`, `payment_method_id`, `delivery_weight`, `token`, `tracking_id`, `is_read`) VALUES
(5, 2566, 'XD-20160328', 2, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 0.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-28 10:08:22', '2016-03-28 10:08:22', '', '', '', 0, 0, 0, '', '', 0),
(6, 6384, 'XD-20160328', 2, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 10425000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-28 10:09:40', '2016-03-28 10:09:40', '', '', '', 0, 0, 1000, '', '', 0),
(7, 4492, 'XD-20160328', 2, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'surabaya', '65323', '1', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'surabaya', '65323', '1', 0, '', '', 0.0000, 4225000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-28 10:13:39', '2016-03-28 10:13:39', '', '', '', 0, 0, 400, '', '', 0),
(8, 8207, 'XD-20160328', 2, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 5100000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-28 10:16:27', '2016-03-28 10:16:27', '', '', '', 0, 0, 400, '', '', 0),
(9, 2869, 'XD-20160328', 2, 0, 'sales', 'dv', 'deo@markdesign.net', '584651561', 'sales', 'dv', '', 'ajasdklaj', '', 'akjdsklja', 'akjsd', '11', '', 'sales', 'dv', '', 'ajasdklaj', '', 'akjdsklja', 'akjsd', '11', 0, '', '', 0.0000, 4200000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-28 13:23:41', '2016-03-28 13:23:41', '', '', '', 0, 0, 400, '', '', 0),
(10, 1650, 'XD-20160329', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test test', '', 'batu', '65656', '11', '', 'deory pandu putra', 'wahyu', '', 'jl test test', '', 'batu', '65656', '11', 0, '', '', 0.0000, 10250000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-29 09:33:57', '2016-03-29 09:33:57', '', '', '', 0, 0, 800, '', '', 0),
(11, 1393, 'XD-20160329', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test test', '', 'batu', '65656', '11', '', 'deory pandu putra', 'wahyu', '', 'jl test test', '', 'batu', '65656', '11', 0, '', '', 0.0000, 5150000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-29 09:42:33', '2016-03-29 09:42:33', '', '', '', 0, 0, 800, '', '', 0),
(12, 5285, 'XD-20160329', 2, 0, 'deory pandu putra', 'wahyu', 'deoryzpandu@gmail.com', '0854646464', 'deory pandu putra', 'wahyu', '', 'jl test test', '', 'batu', '65656', '11', '', 'deory pandu putra', 'wahyu', '', 'jl test test', '', 'batu', '65656', '11', 0, '', 'Your order is being sent', 0.0000, 4650000.0000, 3, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-03-29 09:51:02', '2016-03-29 09:51:02', '', '', '', 0, 0, 800, '', 'SUBGQ00017226516', 0),
(13, 8150, 'XD-20160413', 11, 0, 'chendra', 'cahyadi', 'chendra@markdesign.net', '03160251101', 'chendra', 'cahyadi', '', 'bawean 50', '', 'surabaya', '60246', '11', '', 'chendra', 'cahyadi', '', 'bawean 50', '', 'surabaya', '60246', '11', 0, '', '', 0.0000, 4900000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-04-13 15:00:08', '2016-04-13 15:00:08', '', '', '', 0, 0, 1000, '', '', 0),
(14, 2930, 'LN-20160616', 0, 0, 'Dinda', 'Cholifah Putri', 'dindacholifahputri@gmail.com', '089675783665', 'Dinda', 'Cholifah Putri', '', 'Jl.Brigjen Katamso gg.Anggrek 2 No.27', '', 'Sidoarjo', '61256', '11', '', 'Dinda', 'Cholifah Putri', '', 'Jl.Brigjen Katamso gg.Anggrek 2 No.27', '', 'Sidoarjo', '61256', '11', 0, '', '', 0.0000, 2625000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-06-16 13:34:54', '2016-06-16 13:34:54', '', '', '', 0, 0, 400, '', '', 0),
(15, 3172, 'LN-20160616', 12, 0, 'Dinda', 'Cholifah Putri', 'dindacholifahputri@gmail.com', '089675783665', 'Dinda', 'Cholifah Putri', '', 'Jl.Brigjen Katamso gg.Anggrek 2 No.27', '', 'Sidoarjo', '61256', '11', '', 'Dinda', 'Cholifah Putri', '', 'Jl.Brigjen Katamso gg.Anggrek 2 No.27', '', 'Sidoarjo', '61256', '11', 0, '', 'Your order is being sent', 0.0000, 2650000.0000, 3, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-06-16 13:37:15', '2016-06-16 13:37:15', '', '', '', 0, 0, 600, '', 'MYBOO00220879516', 0),
(16, 7578, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 1400000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:32:03', '2016-07-23 13:32:03', '', '', '', 0, 0, 0, '', '', 0),
(17, 1138, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 577500.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:32:54', '2016-07-23 13:32:54', '', '', '', 0, 0, 0, '', '', 0),
(18, 2204, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 7000000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:33:11', '2016-07-23 13:33:11', '', '', '', 0, 0, 0, '', '', 0),
(19, 5346, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 8400000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:33:31', '2016-07-23 13:33:31', '', '', '', 0, 0, 0, '', '', 0),
(20, 6327, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 23725000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:33:48', '2016-07-23 13:33:48', '', '', '', 0, 0, 13000, '', '', 0),
(21, 8129, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 35100000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:34:07', '2016-07-23 13:34:07', '', '', '', 0, 0, 6000, '', '', 0),
(22, 1715, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 9800000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:34:19', '2016-07-23 13:34:19', '', '', '', 0, 0, 3000, '', '', 0),
(23, 8880, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 13720000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:34:30', '2016-07-23 13:34:30', '', '', '', 0, 0, 4200, '', '', 0),
(24, 2001, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 6120000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:35:09', '2016-07-23 13:35:09', '', '', '', 0, 0, 3000, '', '', 0),
(25, 6959, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 6930000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:35:23', '2016-07-23 13:35:23', '', '', '', 0, 0, 0, '', '', 0),
(26, 1454, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 2887500.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:36:10', '2016-07-23 13:36:10', '', '', '', 0, 0, 0, '', '', 0),
(27, 2754, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 7840000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:36:47', '2016-07-23 13:36:47', '', '', '', 0, 0, 2400, '', '', 0),
(28, 6832, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 44100000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:37:14', '2016-07-23 13:37:14', '', '', '', 0, 0, 9000, '', '', 0),
(29, 1860, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 2125000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:44:54', '2016-07-23 13:44:54', '', '', '', 0, 0, 0, '', '', 0),
(30, 7080, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 1425000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:50:25', '2016-07-23 13:50:25', '', '', '', 0, 0, 0, '', '', 0),
(31, 2504, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 1880000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:50:37', '2016-07-23 13:50:37', '', '', '', 0, 0, 0, '', '', 0),
(32, 8877, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 1965000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:50:57', '2016-07-23 13:50:57', '', '', '', 0, 0, 0, '', '', 0),
(33, 1586, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 12750000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:51:11', '2016-07-23 13:51:11', '', '', '', 0, 0, 0, '', '', 0),
(34, 6842, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 13500000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:51:33', '2016-07-23 13:51:33', '', '', '', 0, 0, 3000, '', '', 0),
(35, 4385, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 12240000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:51:44', '2016-07-23 13:51:44', '', '', '', 0, 0, 6000, '', '', 0),
(36, 7167, 'LN-20160723', 5, 0, 'deory pandu putra', 'wahyu', 'ibnu@markdesign.net', '085646765265', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', '', 'deory pandu putra', 'wahyu', '', 'Jl Martorejo No 113', '', 'Batu', '65323', '11', 0, '', '', 0.0000, 9800000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-07-23 13:51:54', '2016-07-23 13:51:54', '', '', '', 0, 0, 2000, '', '', 0),
(37, 3007, 'PP-20161018', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deory', 'Testing', 'na', 'jl martorejo', '', 'kota', '65323', '6', 0, '', 'test', 0.0000, 25000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2016-10-18 13:27:04', '2016-10-18 13:27:04', '', '', '', 0, 0, 0, '', '', 0),
(38, 5364, 'PP-20170131', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '', '11', 0, '', '', 0.0000, 1197000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-01-31 16:29:47', '2017-01-31 16:29:47', '', '', '', 0, 0, 3000, '', '', 0),
(39, 7564, 'PP-20170131', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '', '11', 0, '', '', 0.0000, 798000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-01-31 16:30:21', '2017-01-31 16:30:21', '', '', '', 0, 0, 2000, '', '', 0),
(40, 5219, 'VT-20170131', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '', '11', 0, '', '', 0.0000, 399000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-01-31 17:29:01', '2017-01-31 17:29:01', '', '', '', 5000, 0, 1000, '', '', 0),
(41, 3777, 'VT-20170131', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '', '11', 0, '', '', 0.0000, 399000.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-01-31 17:29:09', '2017-01-31 17:29:09', '', '', '', 5000, 0, 1000, '', '', 0),
(42, 3020, 'VT-20170210', 0, 0, '', '', 'deo@markdesign.net', '48484984', '', '', '', '', '', '', '', '', '', 'deory', '', '', 'test', '', 'kota', '', 'au', 0, '', '', 0.0000, 13.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-02-10 13:58:39', '2017-02-10 13:58:39', '', '', '', 0, 0, 1200, '', '', 1),
(43, 6987, 'SU-20170216', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '65323', '11', 0, '', '', 0.0000, 24.0000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-02-16 15:17:42', '2017-02-16 15:17:42', '', '', '', 0, 0, 600, '', '', 0),
(44, 9303, 'SU-20170216', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '65323', '11', 0, '', '', 0.0000, 12.5000, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-02-16 15:21:50', '2017-02-16 15:21:50', '', '', '', 0, 0, 600, '', '', 1),
(45, 6093, 'SU-20170216', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '65323', '11', 0, '', '', 0.0000, 7.2300, 1, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-02-16 16:04:17', '2017-02-16 16:04:17', '', '', '', 0, 0, 600, '', '', 1),
(46, 9210, 'SU-20170224', 0, 0, '', '', 'deo@markdesign.net', '085646765265', '', '', '', '', '', '', '', '', '', 'deoryzzz', '', '', 'Jl Martorejo No 113', '', '444', '65323', '11', 0, '', 'Your order is being processed', 0.0000, 43.3800, 2, 0, 0.0000, 0, 0, '', 0.0000, '', '2017-02-24 10:47:49', '2017-02-24 10:47:49', '', '', '', 0, 0, 3600, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `or_order_history`
--

CREATE TABLE `or_order_history` (
  `id` int(15) NOT NULL,
  `member_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(4) NOT NULL,
  `comment` text NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `or_order_history`
--

INSERT INTO `or_order_history` (`id`, `member_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_add`) VALUES
(3, 2, 3, 1, 0, 'Your order DV-20150618-5342 successfully placed with status \"Pending\"', '2015-06-18 09:01:43'),
(4, 2, 4, 1, 0, 'Your order DV-20150618-8189 successfully placed with status \"Pending\"', '2015-06-18 09:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `or_order_product`
--

CREATE TABLE `or_order_product` (
  `id` int(15) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `name` varchar(256) NOT NULL,
  `kode` varchar(256) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `attributes_id` int(11) NOT NULL,
  `attributes_name` varchar(256) NOT NULL,
  `attributes_price` decimal(15,4) NOT NULL,
  `berat` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `or_order_status`
--

CREATE TABLE `or_order_status` (
  `order_status_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `or_order_status`
--

INSERT INTO `or_order_status` (`order_status_id`, `name`) VALUES
(2, 'Processing'),
(3, 'Shipped'),
(7, 'Canceled'),
(5, 'Complete'),
(8, 'Denied'),
(9, 'Canceled Reversal'),
(10, 'Failed'),
(11, 'Refunded'),
(12, 'Reversed'),
(13, 'Chargeback'),
(1, 'Pending'),
(16, 'Voided'),
(15, 'Processed'),
(14, 'Expired'),
(17, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `pdf`
--

CREATE TABLE `pdf` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `file` varchar(200) NOT NULL,
  `size` decimal(10,3) NOT NULL,
  `sort` int(11) NOT NULL,
  `date_input` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pdf`
--

INSERT INTO `pdf` (`id`, `category_id`, `nama`, `image`, `file`, `size`, `sort`, `date_input`) VALUES
(5, 0, 'Coba PDF', 'ef25b-per.jpg', 'Doc1 - Copy2.pdf', 78306.000, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pg_bank`
--

CREATE TABLE `pg_bank` (
  `id` int(25) NOT NULL,
  `id_bank` int(25) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `rekening` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_bank`
--

INSERT INTO `pg_bank` (`id`, `id_bank`, `nama`, `rekening`) VALUES
(1, 2, 'test nama bank', 2147483647),
(2, 4, 'test nama', 928374837);

-- --------------------------------------------------------

--
-- Table structure for table `pg_blog`
--

CREATE TABLE `pg_blog` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) DEFAULT 0,
  `image` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `date_input` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `insert_by` varchar(255) DEFAULT NULL,
  `last_update_by` varchar(255) DEFAULT NULL,
  `writer` int(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_blog`
--

INSERT INTO `pg_blog` (`id`, `topik_id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`) VALUES
(1, 0, '2057e-blog01.jpg', 1, '2020-07-03 11:59:43', '2020-07-06 16:05:47', 'deoryzpandu@gmail.com', 'info@markdesign.net', NULL),
(2, 0, '29f54-blog02.jpg', 1, '2020-07-03 11:59:56', '2020-07-06 16:05:38', 'deoryzpandu@gmail.com', 'info@markdesign.net', NULL),
(3, 0, 'df865-blog03.jpg', 1, '2020-07-03 12:00:09', '2020-07-06 16:05:27', 'deoryzpandu@gmail.com', 'info@markdesign.net', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pg_blog_description`
--

CREATE TABLE `pg_blog_description` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `quote` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_blog_description`
--

INSERT INTO `pg_blog_description` (`id`, `blog_id`, `language_id`, `title`, `content`, `quote`) VALUES
(10, 1, 2, 'Perhatian PT. BMI terhadap keamanan pangan', '<p>Dunia kini sangat menyoroti lingkungan hidup, terutama keberlangsungan keamanan pangan dari sumber laut (Sea Food sustainability). Keamanan pangan adalah perhatian dunia, sehingga berbagai regulasi mulai bergulir dari para pelanggan di dunia. Para importir di negara maju selalu mengikuti trend demi kebaikan bersama masyarakat di dunia dan demi kelangsungan bumi ini. Aneka persyaratan selalu diajukan negara tujuan ekspor, contohnya, pemerintah Eropa memberikan regulasi tentang residu yang ada di udang dan dianjurkan memiliki antiresidu, antibiotika, dan anti logam berat yang kecil. Sistem ini diterapkan pemerintah dengan mengeluarkan sertifikat kelayakan proses yang memiliki tingkatan-tingkatan berlapis. Maka dari itu, jika ingin masuk dan tembus pasar ekspor Eropa harus berada di grade atau tingkatan sertifikasi yang paling tinggi. PT. BMI menyikapi persyaratan ini bukan sebagai hambatan ekspor, namun sebaliknya menjadi sebuah nilai positif di mana kehidupan yang lebih baik akan terwujud dengan lebih baik pula.</p><p>Dalam langkah untuk menyikapi hal ini, PT. BMI menjalin kerjasama dengan Universitas Brawijaya dalam melakukan penelitian dampak lingkungan dari proses produksinya. “Kami juga memikirkan hewan apa saja yang ada di lingkungan kami, apakah mereka tercemar atau tidak. Kami keluarkan cost sangat tinggi untuk kerjasama ini demi penelitian terkait dampak lingkungan,” ungkap Aris sebagai Direktur Utama PT. BMI. Hal ini menjadi perhatian karena isu lingkungan dipandang sangat penting bagi negara-negara maju seperti AS dan Eropa.</p>', NULL),
(9, 2, 2, 'Eksistensi aneka merk dari PT. BMI di dunia', '<p>Di pasar ekspor, PT. BMI memiliki merek dagang yaitu Ocean Ruby yang telah menjadi pemain atas / primadona untuk produk udang. Selain itu merek Java Base dan Menara Seafood juga sangat diminati oleh para konsumen dari mancanegara. Tahun 2016, ekspor BMI sebesar US$ 250 juta, dan selalu naik dari tahun ke tahun hingga tahun 2019. Rata-rata pertumbuhan adalah 5% hingga 10%.</p><p>Selama PT. BMI menjadi eksportir, berbagai pengalaman telah membuat PT. BMI menjadi mahir serta fleksibel dengan peraturan di setiap negara. Negara-negara importir tidak memberlakukan aturan-aturan atau regulasi yang terlalu memberatkan bagi kami eksportir. Untuk Eropa yang sedikit berbeda, persyaratan lebih ketat namun ditujukan bagi pemerintah bukan untuk eksportir langsung. Mereka yang akan menyeleksi apakah layak makanan ekspor masuk.</p><p>PT. BMI dengan berbagai reputasi merknya selalu menjaga kepuasan pelanggan dan pasokan bahan baku sebagai kunci dalam memenangkan hubungan jangka panjang. Fokus menangkap pelanggan besar pada negara tujuan ekspor adalah strategi yang dilakukan oleh PT. BMI. Dengan dominasi produk di importir besar di Amerika, maka eksposur produk PT. BMI menjadi lebih bagus dan menarik perhatian importir lain di negara yang sama maupun negara lain.</p><p>Dengan memaksimalkan kualitas bahan baku sebagai komoditi utama, PT. BMI berhasil menjadi salah satu pemasok terbaik dunia. Manajemen produksi yang baik dan benar harus dijalankan, serta tata cara pengelolaan tambak, penebaran benih, serta kemampuan menjaga kelangsungan panen harus diperhatikan agar kualitas akhir maksimal.</p>', NULL),
(8, 3, 2, 'Keberhasilan Ekspor Seafood BMI di dunia', '<p>Kontribusi utama PT Bumi Menara Internusa (BMI) di bidang hasil laut memberi kontribusi yang baik pada devisa negara. Hasil ekspor BMI termasuk diantaranya adalah udang, kepiting, dan aneka ikan. Tiap tahunnya nilai ekspor ini mampu mencetak triliunan rupiah. Prestasi BMI ini salah satunya pernah diapresiasi dengan penghargaan Primaniyarta Award 2017 kategori Eksportir Berkinerja dari Kementrian Perdangangan RI.</p><p>BMI mempersembahkan kepada para konsumennya, berbagai produk dari seafood mentah hingga seafood hasil laut siap saji yang praktis dan mudah dinikmati semua pasar. Saat ini yang menjadi konsumen pasar utama BMI adalah Amerika Serikat dan negara-negara Eropa, Australia, China serta negara Asia lainnya. Menurut Aris Utama, Direktur Utama dari PT BMI, penjualan terbesar berasal dari hasil laut yakni udang yang mencapai 70%. Dari segi volume, ekspor BMI selalu mengalami kenaikan terus tiap tahunnya. Terkadang kenaikannya memang tidak terlalu signifikan, namun secara garis besar gradual selalu naik. PT. BMI menargetkan setiap tahunnya peningkatan ekspor adalah 10%.</p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pg_faq`
--

CREATE TABLE `pg_faq` (
  `id` int(20) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_faq`
--

INSERT INTO `pg_faq` (`id`, `status`) VALUES
(1, 1),
(2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pg_faq_description`
--

CREATE TABLE `pg_faq_description` (
  `id` int(11) NOT NULL,
  `faq_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_faq_description`
--

INSERT INTO `pg_faq_description` (`id`, `faq_id`, `language_id`, `question`, `answer`) VALUES
(1, 1, 1, 'test tanya indo', '<p>\r\n test jawab indo\r\n</p>'),
(2, 1, 2, 'test tanya?', '<p>\r\n test jawab 1\r\n</p>'),
(3, 2, 2, 'test question 1', '<p>\r\n test answer 1\r\n</p>');

-- --------------------------------------------------------

--
-- Table structure for table `pg_list_bank`
--

CREATE TABLE `pg_list_bank` (
  `id` int(50) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `label` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_list_bank`
--

INSERT INTO `pg_list_bank` (`id`, `nama`, `label`) VALUES
(1, 'bank_mega', 'Bank Mega'),
(2, 'bca', 'BCA'),
(3, 'bca_syariah', 'BCA Syariah'),
(4, 'bii', 'BII'),
(5, 'bni', 'BNI'),
(6, 'bni_syariah', 'BNI Syariah'),
(7, 'bri', 'BRI'),
(8, 'bri_syariah', 'BRI Syariah'),
(9, 'cimb_niaga', 'CIMB Niaga'),
(10, 'cimb_niaga_syariah', 'CIMB Niaga Syariah'),
(11, 'citibank', 'Citibank'),
(12, 'danamon', 'Danamon'),
(13, 'hsbc', 'HSBC'),
(14, 'mandiri', 'Mandiri'),
(15, 'mandiri_syariah', 'Mandiri Syariah'),
(16, 'money_gram', 'Money Gram'),
(17, 'muamalat', 'Muamalat'),
(18, 'paypal', 'Paypal'),
(19, 'permata', 'Permata'),
(20, 'visa', 'Visa'),
(21, 'western_union', 'Western Union');

-- --------------------------------------------------------

--
-- Table structure for table `pg_pages`
--

CREATE TABLE `pg_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `type` int(1) NOT NULL DEFAULT 1,
  `group` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_pages`
--

INSERT INTO `pg_pages` (`id`, `name`, `type`, `group`) VALUES
(1, 'testimonial', 0, 'testimonial'),
(2, 'articles', 0, 'blog'),
(3, 'about', 0, 'static'),
(4, 'contact', 0, 'static'),
(5, 'faq', 0, 'faq'),
(6, 'how_to_shop', 0, 'static'),
(7, 'payment_confirmation', 0, 'static'),
(8, 'bank', 0, 'bank');

-- --------------------------------------------------------

--
-- Table structure for table `pg_pages_description`
--

CREATE TABLE `pg_pages_description` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `page_name` varchar(225) NOT NULL,
  `content` longtext NOT NULL,
  `meta_title` varchar(225) NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_pages_description`
--

INSERT INTO `pg_pages_description` (`id`, `page_id`, `language_id`, `page_name`, `content`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(11, 4, 1, 'Kontak Kami', '<p>\r\n  Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Contact\r\n</p>\r\n<p>\r\n    CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Kontak Kami', 'edit di admin panel -> Pages -> Contact', 'edit di admin panel -> Pages -> Contact'),
(12, 4, 2, 'Contact', '<p>\r\n  Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Contact\r\n</p>\r\n<p>\r\n    CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Contact', 'edit di admin panel -> Pages -> Contact', 'edit di admin panel -> Pages -> Contact'),
(13, 6, 1, 'How To Shop', '<p>\r\n  Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; How To Shop\r\n</p>\r\n<p>\r\n     CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'How To Shop', 'Edit di admin panel -> Pages -> How To Shop', 'Edit di admin panel -> Pages -> How To Shop'),
(14, 6, 2, 'How To Shop', '<p>\r\n  Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; How To Shop\r\n</p>\r\n<p>\r\n     CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'How To Shop', 'Edit di admin panel -> Pages -> How To Shop', 'Edit di admin panel -> Pages -> How To Shop'),
(15, 7, 1, 'Konfirmasi Pembayaran', '<p>\r\n       Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Payment Confirmation\r\n</p>\r\n<p>\r\n     CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Konfirmasi Pembayaran', 'Edit di admin panel -> Pages -> Payment Confirmation', 'Edit di admin panel -> Pages -> Payment Confirmation'),
(16, 7, 2, 'Payment Confirmation', '<p>\r\n      Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; Payment Confirmation\r\n</p>\r\n<p>\r\n     CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa  lain di website anda di admin panel -&gt; General Setting -&gt;  Language(Bahasa)\r\n</p>', 'Payment Confirmation', 'Edit di admin panel -> Pages -> Payment Confirmation', 'Edit di admin panel -> Pages -> Payment Confirmation'),
(17, 3, 2, 'About', '<p>\r\n       Untuk mengedit halaman ini anda bisa melakukannya dengan masuk ke admin panel -&gt; Pages -&gt; About\r\n</p>\r\n<p>\r\n    CMS Ini juga support dalam dual bahasa, anda bisa menambahkan bahasa lain di website anda di admin panel -&gt; General Setting -&gt; Language(Bahasa)\r\n</p>', 'About', 'edit di admin panel -> Pages -> About', 'edit di admin panel -> Pages -> About');

-- --------------------------------------------------------

--
-- Table structure for table `pg_testimonial`
--

CREATE TABLE `pg_testimonial` (
  `id` int(25) NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `testimonial` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_testimonial`
--

INSERT INTO `pg_testimonial` (`id`, `name`, `email`, `testimonial`, `status`, `date`) VALUES
(1, 'Ibnu', 'ibnu@markdesign.net', '', 1, '2014-07-14 09:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `pg_testimonial_description`
--

CREATE TABLE `pg_testimonial_description` (
  `id` int(11) NOT NULL,
  `testimonial_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_testimonial_description`
--

INSERT INTO `pg_testimonial_description` (`id`, `testimonial_id`, `language_id`, `content`) VALUES
(8, 1, 2, 'test'),
(7, 1, 1, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `pg_type_letak`
--

CREATE TABLE `pg_type_letak` (
  `id` int(11) NOT NULL,
  `letak` varchar(225) NOT NULL,
  `page_id` int(11) NOT NULL,
  `tampil` int(11) NOT NULL,
  `sort` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pg_type_letak`
--

INSERT INTO `pg_type_letak` (`id`, `letak`, `page_id`, `tampil`, `sort`) VALUES
(177, 'header', 2, 1, 1),
(178, 'header', 6, 1, 2),
(179, 'header', 4, 1, 3),
(180, 'header', 1, 0, 4),
(181, 'header', 3, 0, 5),
(182, 'header', 5, 0, 6),
(183, 'header', 7, 0, 7),
(184, 'header', 8, 0, 8),
(185, 'footer', 3, 1, 1),
(186, 'footer', 6, 1, 2),
(187, 'footer', 4, 1, 3),
(188, 'footer', 1, 0, 4),
(189, 'footer', 2, 0, 5),
(190, 'footer', 5, 0, 6),
(191, 'footer', 7, 0, 7),
(192, 'footer', 8, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `prd_brand`
--

CREATE TABLE `prd_brand` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_brand`
--

INSERT INTO `prd_brand` (`id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `logo`, `type`) VALUES
(5, '7e6b9-1.png', 1, '2018-12-03 12:36:55', '2018-12-03 12:36:55', 'info@markdesign.net', 'info@markdesign.net', '7e6b9-1.png', 1),
(6, '00cac-7.png', 1, '2018-12-03 12:42:34', '2018-12-03 12:42:34', 'info@markdesign.net', 'info@markdesign.net', '00cac-7.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prd_brand_description`
--

CREATE TABLE `prd_brand_description` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_brand_description`
--

INSERT INTO `prd_brand_description` (`id`, `brand_id`, `language_id`, `title`, `content`) VALUES
(2, 1, 2, 'LONCIN', '<p>\r\n  Lorem ipsum dolor sit amet.consectur adipiscing elit.                integer sit amet sem id dolor interdum malesuada in ut                mi. Vestibulum sed tempor nisi, a varius arcu. Nam vel odio sit amet erat facilisis elementum.\r\n</p>'),
(7, 2, 2, 'SDF', '<p>\r\n  Lorem ipsum dolor sit amet.consectur adipiscing elit.                integer sit amet sem id dolor interdum malesuada in ut                mi. Vestibulum sed tempor nisi, a varius arcu. Nam vel odio sit amet erat facilisis elementum.\r\n</p>'),
(8, 3, 2, 'Maxter', '<p>\r\n   Lorem ipsum dolor sit amet.consectur adipiscing elit.                integer sit amet sem id dolor interdum malesuada in ut                mi. Vestibulum sed tempor nisi, a varius arcu. Nam vel odio sit amet erat facilisis elementum.\r\n</p>'),
(6, 4, 2, 'Deutz', '<p>\r\n  Lorem ipsum dolor sit amet.consectur adipiscing elit.                integer sit amet sem id dolor interdum malesuada in ut                mi. Vestibulum sed tempor nisi, a varius arcu. Nam vel odio sit amet erat facilisis elementum.\r\n</p>'),
(9, 5, 2, 'Yasuka', ''),
(10, 6, 2, 'Panasonic', '');

-- --------------------------------------------------------

--
-- Table structure for table `prd_category`
--

CREATE TABLE `prd_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `image2` varchar(200) NOT NULL,
  `image3` varchar(200) NOT NULL,
  `type` varchar(100) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category`
--

INSERT INTO `prd_category` (`id`, `parent_id`, `sort`, `image`, `image2`, `image3`, `type`, `data`) VALUES
(1, 0, 0, 'd30ef-Layer-47.jpg', '', '', 'category', ''),
(2, 0, 0, '47278-Layer-48.jpg', '', '', 'category', ''),
(3, 0, 0, 'feac0-Layer-33.jpg', '', '', 'category', ''),
(4, 0, 0, '9370e-Layer-35.jpg', '', '', 'category', '');

-- --------------------------------------------------------

--
-- Table structure for table `prd_category_description`
--

CREATE TABLE `prd_category_description` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category_description`
--

INSERT INTO `prd_category_description` (`id`, `category_id`, `language_id`, `name`, `desc`, `data`) VALUES
(1, 1, 2, 'Shrimp', '', ''),
(2, 2, 2, 'Alaska Salmon', '', ''),
(3, 3, 2, 'Yellow Fin Tunna', '', ''),
(4, 4, 2, 'Oyster', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prd_category_product`
--

CREATE TABLE `prd_category_product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_category_product`
--

INSERT INTO `prd_category_product` (`id`, `category_id`, `product_id`) VALUES
(2, 2, 1),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prd_product`
--

CREATE TABLE `prd_product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `harga` decimal(11,2) NOT NULL,
  `harga_coret` decimal(11,2) NOT NULL,
  `harga_retail` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `terbaru` int(11) NOT NULL,
  `terlaris` int(11) NOT NULL,
  `out_stock` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `data` text NOT NULL,
  `tag` text DEFAULT NULL,
  `onsale` int(5) DEFAULT NULL,
  `urutan` int(5) DEFAULT NULL,
  `rekomendasi` int(5) DEFAULT NULL,
  `turun_harga` int(5) DEFAULT NULL,
  `image2` varchar(225) DEFAULT NULL,
  `filter` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_product`
--

INSERT INTO `prd_product` (`id`, `category_id`, `brand_id`, `image`, `kode`, `harga`, `harga_coret`, `harga_retail`, `stock`, `berat`, `terbaru`, `terlaris`, `out_stock`, `status`, `date`, `date_input`, `date_update`, `data`, `tag`, `onsale`, `urutan`, `rekomendasi`, `turun_harga`, `image2`, `filter`) VALUES
(1, 0, 0, '3167b-660x660.png', 'asdf', 0.00, 0.00, 0, 0, 0, 0, 0, 0, 1, '2020-03-16 10:39:13', '2020-03-16 10:39:13', '2020-03-18 14:48:18', 'N;', 'category=Alaska Salmon, category=2, category=Shrimp, category=1,', NULL, NULL, NULL, NULL, NULL, 'category=Alaska Salmon||category=2||category=Shrimp||category=1||');

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_attributes`
--

CREATE TABLE `prd_product_attributes` (
  `id` int(11) NOT NULL,
  `id_str` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute` varchar(200) NOT NULL,
  `stock` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_color`
--

CREATE TABLE `prd_product_color` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `image_color` varchar(200) NOT NULL,
  `label` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_description`
--

CREATE TABLE `prd_product_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `desc` text NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_key` text NOT NULL,
  `note` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prd_product_description`
--

INSERT INTO `prd_product_description` (`id`, `product_id`, `language_id`, `name`, `subtitle`, `desc`, `meta_title`, `meta_desc`, `meta_key`, `note`) VALUES
(2, 1, 2, 'Vaname Shrimp - Head & Tail On', '', '<p>Integer fermentum neque eget sapien euismod, et scelerisque neque faucibus. Morbi condimentum bibendum consequat. Aliquam tincidunt quis eros sed aliquam. Proin eu facilisis sapien. Nulla eget ex sit amet massa venenatis laoreet. Mauris auctor neque id dignissim venenatis.</p>', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prd_product_image`
--

CREATE TABLE `prd_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sertifikasi`
--

CREATE TABLE `sertifikasi` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `images_icon` varchar(225) NOT NULL,
  `images_big` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sertifikasi`
--

INSERT INTO `sertifikasi` (`id`, `name`, `description`, `images_icon`, `images_big`) VALUES
(2, 'Agen tunggal resmi di Indonesia', '', 'edf8c-XADO-certificate-thumbc0cdba.jpg', 'edf8c-XADO-certificate-10bef2.jpg'),
(3, 'Technischer Uberwachungs-Verein', 'The certificate states the efficiency and certifies the XADO gel-revitalizant for gasoline engines effect', '63a71-XADO-certificate-thumb6f863e.jpg', '1af50-XADO-certificate-d20515.jpg'),
(4, 'Kristen Petra University Surabaya Certificate', 'XADO Gel For Diesel Engine', '97272-XADO-certificate-thumb0c1964.jpg', '4c78b-XADO-certificate-efb9bb.jpg'),
(6, 'API certificate', 'API - American Petroleum Institute', 'b1712-XADO-certificate-thumbf05da4.jpg', 'b1712-XADO-certificate-0444a4.jpg'),
(7, 'MAN Certificate', 'Approval For Engine Oil XADO 80W-90 GL-3/4/5', 'f28aa-XADO-certificate-thumbbb60d6.gif', 'f28aa-XADO-certificate-a0d9dd.jpg'),
(8, 'MAN Certificate', 'Approval For Engine Oil XADO 10W-40 Diesel Truck', 'a23f8-XADO-certificate-thumbbb60d6.gif', 'a23f8-XADO-certificate-fecbed.jpg'),
(9, 'VOLVO Certificate', 'Approval For Engine Oil XADO 15W-40 SL/CI-4 & XADO 10W-40 SL/CI-4', '2942f-XADO-certificate-thumbd89b2a.jpg', '2942f-XADO-certificate-db514b.jpg'),
(10, 'VOLVO Certificate', 'Approval For Engine Oil XADO 15W-40 CI-4 Diesel', '931e7-XADO-certificate-thumbd89b2a.jpg', '931e7-XADO-certificate-e931a6.jpg'),
(11, 'VOLVO Certificate', 'Approval For Engine Oil XADO Atomic Oil 15W-40 SM/CJ-4', 'b80dc-XADO-certificate-thumbd89b2a.jpg', 'b80dc-XADO-certificate-120754.jpg'),
(12, 'Daimler-Benz Certificate', 'Approval For Engine Oil XADO 5W-40 SM/CF', 'ab745-XADO-certificate-thumba2f7af.jpg', 'ab745-XADO-certificate-f97b75.jpg'),
(13, 'General Motor Certificate', 'Approval For Engine Oil XADO 5W-40 City Line SL/CF', 'fdfc0-XADO-certificate-thumb109fe4.jpg', 'fdfc0-XADO-certificate-a26364.jpg'),
(14, 'BMW certificate', 'Approval for Engine Oil \"XADO 5W-40 SM/CF\"', '90b56-XADO-certificate-thumbc23299.jpg', '90b56-XADO-certificate-1a4bd9.jpg'),
(15, 'VolksWagen Certificate', 'Approval For Engine Oil XADO SL/CF SAE 5W-40', 'cfe0e-XADO-certificate-thumbc44858.jpg', 'cfe0e-XADO-certificate-09c146.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `label` varchar(200) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(100) NOT NULL,
  `hide` int(11) NOT NULL,
  `group` varchar(100) NOT NULL,
  `dual_language` enum('n','y') NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `name`, `label`, `value`, `type`, `hide`, `group`, `dual_language`, `sort`) VALUES
(1, 'default_meta_title', 'Title', '', 'text', 0, 'default_meta', 'y', 1),
(2, 'default_meta_keywords', 'Keywords', '', 'textarea', 0, 'default_meta', 'y', 2),
(3, 'default_meta_description', 'Description', '', 'textarea', 0, 'default_meta', 'y', 3),
(4, 'google_tools_webmaster', 'Google Webmaster Code', '', 'textarea', 0, 'google_tools', 'n', 4),
(5, 'google_tools_analytic', 'Google Analytic Code', '', 'textarea', 0, 'google_tools', 'n', 5),
(6, 'purechat_status', 'Show Hide Widget', '', 'select', 0, 'purechat', 'n', 1),
(7, 'purechat_code', 'PureChat Code', '', 'textarea', 0, 'purechat', 'n', 1),
(8, 'invoice_start_number', 'Invoice Start Number', '1000', 'text', 0, 'invoice', 'n', 0),
(9, 'invoice_increment', 'Invoice Increment', '5', 'text', 0, 'invoice', 'n', 0),
(10, 'invoice_auto_cancel_after', 'Invoice Auto Cancel After', '72', 'text', 0, 'invoice', 'n', 0),
(11, 'lang_deff', 'Language Default', 'en', 'text', 0, 'data', 'n', 0),
(12, 'email', 'Email Form', 'info@bmi.co.id', 'text', 0, 'data', 'n', 1),
(314, 'home_slide_text_title', 'Slide Title', '', 'text', 0, 'data', 'y', 0),
(315, 'home_slide_text_content', 'Slide Content', '', 'text', 0, 'data', 'y', 0),
(316, 'home1_title', 'Home 1 Title', '', 'text', 0, 'data', 'y', 0),
(317, 'home1_subcontent', 'Sub 1 Content', '', 'text', 0, 'data', 'y', 0),
(318, 'home2_title', 'Home 2 - Title', '', 'text', 0, 'data', 'y', 0),
(319, 'home2_content', 'Home 2 - Content', '', 'text', 0, 'data', 'y', 0),
(320, 'home2_chl_title_top', 'Home 3 - Title', '', 'text', 0, 'data', 'y', 0),
(321, 'home2_chl_number_1', 'Input Numbers', '16', 'text', 0, 'data', 'n', 0),
(322, 'home2_chl_txt_1', 'Input Texts', 'PROCESSING FACTORY', 'text', 0, 'data', 'n', 0),
(323, 'home2_chl_number_2', 'Input Numbers', '5000+', 'text', 0, 'data', 'n', 0),
(324, 'home2_chl_txt_2', 'Input Texts', 'HUMAN RESOURCE', 'text', 0, 'data', 'n', 0),
(325, 'home2_chl_number_3', 'Input Numbers', '72 Tons', 'text', 0, 'data', 'n', 0),
(326, 'home2_chl_txt_3', 'Input Texts', 'DAILY PROCESSING', 'text', 0, 'data', 'n', 0),
(327, 'home2_chl_number_4', 'Input Numbers', '3000 Tons', 'text', 0, 'data', 'n', 0),
(328, 'home2_chl_txt_4', 'Input Texts', 'MONTHLY OUTPUT', 'text', 0, 'data', 'n', 0),
(329, 'about_hero_image', 'Image', '50b2acdf6bcover-about.jpg', 'image', 0, 'data', 'n', 0),
(330, 'about_hero_title', 'Title', '', 'text', 0, 'data', 'y', 0),
(331, 'about1_image', 'Image', '9c41a9d5d4about-ban-1.jpg', 'image', 0, 'data', 'n', 0),
(332, 'about1_content', 'Content', '', 'text', 0, 'data', 'y', 0),
(333, 'about2_banners_1', 'Image Banner', 'e7d3b3fb6cabout-ban-2.jpg', 'image', 0, 'data', 'n', 0),
(334, 'about2_banners_2', 'Image Banner', 'afc31c32b8about-ban-3.jpg', 'image', 0, 'data', 'n', 0),
(335, 'about2_banners_3', 'Image Banner', 'ad74003146about-ban-4.jpg', 'image', 0, 'data', 'n', 0),
(336, 'about2_banners_4', 'Image Banner', '8ab4d20b48gambar1 (1).png', 'image', 0, 'data', 'n', 0),
(337, 'about3_content', 'Content', '', 'text', 0, 'data', 'y', 0),
(338, 'about3_image', 'Image', '526e4ea22eabout-ban-5.jpg', 'image', 0, 'data', 'n', 0),
(339, 'about4_title', 'Title', '', 'text', 0, 'data', 'y', 0),
(340, 'about4_subcontent', 'Sub Content', '', 'text', 0, 'data', 'y', 0),
(341, 'about4_hl_titles_1', 'Title', '', 'text', 0, 'data', 'y', 0),
(342, 'about4_hl_desc_1', 'Description', '', 'text', 0, 'data', 'y', 0),
(343, 'about4_hl_picture_1', 'Image', '1881a518e0about_16.jpg', 'image', 0, 'data', 'n', 0),
(344, 'about4_hl_titles_2', 'Title', '', 'text', 0, 'data', 'y', 0),
(345, 'about4_hl_desc_2', 'Description', '', 'text', 0, 'data', 'y', 0),
(346, 'about4_hl_picture_2', 'Image', 'b4f0ee1b88about_18.jpg', 'image', 0, 'data', 'n', 0),
(347, 'about4_hl_titles_3', 'Title', '', 'text', 0, 'data', 'y', 0),
(348, 'about4_hl_desc_3', 'Description', '', 'text', 0, 'data', 'y', 0),
(349, 'about4_hl_picture_3', 'Image', '68d29612adabout_22.jpg', 'image', 0, 'data', 'n', 0),
(350, 'about4_hl_titles_4', 'Title', '', 'text', 0, 'data', 'y', 0),
(351, 'about4_hl_desc_4', 'Description', '', 'text', 0, 'data', 'y', 0),
(352, 'about4_hl_picture_4', 'Image', '53c4e7690dabout_24.jpg', 'image', 0, 'data', 'n', 0),
(353, 'quality_hero_image', 'Image', '0904f3ae1bcover-quality.jpg', 'image', 0, 'data', 'n', 0),
(354, 'quality_hero_title', 'Title', '', 'text', 0, 'data', 'y', 0),
(355, 'about2_content', 'Content', '', 'text', 0, 'data', 'y', 0),
(356, 'about2_image', 'Image', '', 'image', 0, 'data', 'n', 0),
(357, 'quality4_title', 'Title', '', 'text', 0, 'data', 'y', 0),
(358, 'quality4_subcontent', 'Sub Content', '', 'text', 0, 'data', 'y', 0),
(359, 'quality4_hl_titles_1', 'Title', '', 'text', 0, 'data', 'y', 0),
(360, 'quality4_hl_picture_1', 'Image', '3e0615bbfdLayer-41.png', 'image', 0, 'data', 'n', 0),
(361, 'quality4_hl_titles_2', 'Title', '', 'text', 0, 'data', 'y', 0),
(362, 'quality4_hl_picture_2', 'Image', 'f615e105bfquality-icn-2.png', 'image', 0, 'data', 'n', 0),
(363, 'quality4_hl_titles_3', 'Title', '', 'text', 0, 'data', 'y', 0),
(364, 'quality4_hl_picture_3', 'Image', '8dc4e23b62Layer-40.png', 'image', 0, 'data', 'n', 0),
(365, 'quality4_hl_titles_4', 'Title', '', 'text', 0, 'data', 'y', 0),
(366, 'quality4_hl_picture_4', 'Image', '4c7161dd8fquality-icn-4.png', 'image', 0, 'data', 'n', 0),
(367, 'contact_hero_image', 'Image', '7b2c9f3ea9cover-contact.jpg', 'image', 0, 'data', 'n', 0),
(368, 'contact_hero_title', 'Title', '', 'text', 0, 'data', 'y', 0),
(369, 'contact_hero_subtitle', 'Sub Title', '', 'text', 0, 'data', 'y', 0),
(370, 'contact_phone', 'Phone', '62 21 590 7777', 'text', 0, 'data', 'n', 0),
(371, 'contact_email', 'Email Website', 'info@bmi.co.id', 'text', 0, 'data', 'n', 0),
(372, 'contact_wa', 'Contact Whatsapp', '6281 6480 7016', 'text', 0, 'data', 'n', 0),
(373, 'contact1_pict', 'Picture Location', 'cda2cd7b5cLayer-38.jpg', 'image', 0, 'data', 'n', 0),
(374, 'contact1_desc', 'Description', '', 'text', 0, 'data', 'y', 0),
(375, 'contact2_pict', 'Picture Location', '150789e852Layer-39.jpg', 'image', 0, 'data', 'n', 0),
(376, 'contact2_desc', 'Description', '', 'text', 0, 'data', 'y', 0),
(377, 'quality1_image', 'Image', 'aaf0bb4319quality-ban-1.jpg', 'image', 0, 'data', 'n', 0),
(378, 'quality1_content', 'Content', '', 'text', 0, 'data', 'y', 0),
(379, 'quality2_content', 'Content', '', 'text', 0, 'data', 'y', 0),
(380, 'quality2_image', 'Image', '5f88b10062quality-ban-2.jpg', 'image', 0, 'data', 'n', 0),
(381, 'quality3_image', 'Image', '4a3170d038quality-ban-3.jpg', 'image', 0, 'data', 'n', 0),
(382, 'quality3_content', 'Content', '', 'text', 0, 'data', 'y', 0),
(383, 'about_visi', 'Content Vision', '', 'text', 0, 'data', 'y', 0),
(384, 'about_misi', 'Content Mission', '', 'text', 0, 'data', 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting_description`
--

CREATE TABLE `setting_description` (
  `id` int(11) NOT NULL,
  `setting_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_description`
--

INSERT INTO `setting_description` (`id`, `setting_id`, `language_id`, `value`) VALUES
(4, 1, 2, 'PT. Bumi Menara Internusa'),
(5, 3, 2, 'PT. Bumi Menara Internusa'),
(6, 2, 2, 'PT. Bumi Menara Internusa'),
(14, 314, 2, 'Leading The Indonesian Shrimp Industries Since 1981'),
(15, 315, 2, '<p>We are proud of the consistently high quality of the shrimp and other seafood products we offer. We help manage and accompany shrimp farmers, giving the best nurturing advise, observe when they are harvesting and delivered to our processing factories.\r\n</p>'),
(16, 316, 2, 'Our Seafood Products'),
(17, 317, 2, '<p>PT. Bumi Menara Internusa is supplying a wide range of shrimp, fish, crabs and value added seafood products from Indonesia, including raw and cooked sea food products such as White Shrimp, Squid, Cuttlefish, Baby Octopus, Seafood Mix, Seafood Skewers, Tuna Loins and many more.</p>'),
(18, 318, 2, ''),
(19, 319, 2, ''),
(20, 320, 2, 'PT. Bumi Menara Internusa Facts & Highlights'),
(34, 330, 2, 'About Us'),
(35, 332, 2, '<h3>Who we are</h3><h5>Located in Indonesia, PT Bumi Menara Internusa (BMI) serves the world with quality and nutritious seafood products ranging from fresh shrimp and crab to species of fish. Benefiting from two decades of experience, BMI presently operates a number of facilities in Indonesia to fulfill the world’s demand for quality seafood products.</h5><p>Maintaining its strong commitment towards customer satisfaction, BMI embraces the most up to date food safety systems and is fully supported by comprehensive and modern food processing technology. BMI is one of the leading processors and exporters of various seafood products which adhere to the strictest international standards, thus making BMI a reliable and preferred partner for worldwide seafood importers and customers.\r\n</p>'),
(36, 337, 2, '<h3>Our Strength</h3><h5>Since established, PT Bumi Menara Internusa continues to evolve and keeping up the pace with the never ending innovation in the world of Seafood commodity products and processing industry.</h5><p>During all these years our founders and our professional team has created series of infrastructures and facilities to grow and increase the industry capacity - Experience counts!</p><p>With extensive and commited after sales service, fully traceable process, also modern supply chain management, BMI is here to assure the long run of your business continuity.  Everything we do at BMI is guided by our vision to ensure that we all go the extra mile to help our customers reach the full potential of their operationals.</p>'),
(37, 339, 2, 'Culture & Work Philosophy'),
(38, 340, 2, '<h5>PT. Bumi Menara Internusa is supplying a wide range of shrimp, fish, crabs and value added seafood products from Indonesia, including raw and cooked sea food products such as White Shrimp, Squid, Cuttlefish, Baby Octopus, Seafood Mix, Seafood Skewers, Tuna Loins and many more.\r\n</h5>'),
(39, 341, 2, 'Improvement'),
(40, 342, 2, 'Continuous improvement is the culture we hold most, as we pride ourself to be at the highest standard amongst the others. We aim to exceed our customer\'s satisfaction on each delivery made by working more efficiently and faster everytime.'),
(41, 344, 2, 'Innovation'),
(42, 345, 2, 'We will do everything we can to complete every task faster, we encourages the use of processes that are repeatable, we always build smarter template of acts to be standardize and systemize in order to refine the process. '),
(43, 347, 2, 'Teamwork'),
(44, 348, 2, 'BMI people are collectivists who believe in teamwork - the goals of the team are the goals of the individual. Teamwork success in BMI is greatly rewarded and recognized. BMI is creating a work culture that values collaboration.'),
(45, 350, 2, 'Punctuality'),
(46, 351, 2, 'In an environment and factory working operation where time and punctuality are valued highly, our people understand and believe that thinking, planning decisions and fast actions in a collective and cooperative way are the keys to be ahead of time.'),
(47, 354, 2, 'Quality & Process'),
(48, 355, 2, ''),
(49, 357, 2, 'We Go Extra Miles For Quality & Excellence'),
(50, 358, 2, '<h5>PT. Bumi Menara Internusa believe that our customers deserves only the best, and by doing that so, we will gain trust and lasting bond - a long term business relationship.</h5>'),
(51, 359, 2, 'Best Raw Commodity'),
(52, 361, 2, 'Best QC Team'),
(53, 363, 2, 'Best Testing Facility'),
(54, 365, 2, 'Best Enterprise Technology'),
(55, 378, 2, '<h3>In BMI, Quality Is In Each Step Of The Production Processing</h3><h5>Quality control process at PT Bumi Menara Internusa Machines Industry starts at the very beginning. For our shrimp customers, we documented each step of the process starting from the commodity materialising, preparation and processing until delivery. </h5><p>The production and processing at BMI uses smart and efficient technology powered by SAP so that we can bring better transparency to our customers. We have the biggest passion to be the best in the industry, therefore we are aware and willing to invest on quality control department.\r\n</p><p>Our quality teams will stop at nothing to ensure that each process are done with quality for customer benefit. Every fine detail is studied meticulously.\r\n</p>'),
(56, 379, 2, '<h3>Quality Sourcing</h3><p>We consult, educate shrimp farmers and even treat the pond with necessary treatments to ensure farmers can grow the best commodity. We also invest in building our own shrimp ponds to supply the never ending demands.</p><p>For other commodities such as fish, crabs and other sea food products, we are meticulous with the origin of the commodity, how they were bred, how they were harvested, and how they were delivered to our facility. We are concerned with not just about how fresh the commodities are, but we are taking attention towards animal cruelty, crimes against the environment, the preservation of nature and fauna. We’re aiming for profit but we care more on how we could help prevent the earth from disaster, and how we could preserve for our children.</p><h5></h5>'),
(57, 382, 2, '<h3>Quality Processing</h3><p>We at PT Bumi Menara Internusa are always adapting and embracing “change” and “trends”, that’s why we have the deppest thirst when it comes to new technology. Anything that could maximise the outcome quality and outsmart the previous process will be our investment priority checklist. We always push ourselves far beyond our limit, we are hard to ourselves in order to bring ultimate satisfactory to our consumers.</p><p>We care about our customers and we know how important to get clear trace of production progress - therefore we integrate our business process with the best enterprise technology to provide real-time tracking and provide product tracebility.</p><p>The continuous innovation in PT Bumi Menara Internusa is a never ending journey and it’s a way of life for us - Whether it’s the latest technology or radical ideas for product processing, we’re always pushing ourselves to do more, and do it better.</p><h5><img src=\"http://localhost/bmi/asset/images/logo-sap.png\" alt=\"\"><br></h5>'),
(58, 368, 2, 'Contact Us'),
(59, 369, 2, ''),
(60, 374, 2, '<h4>Surabaya</h4><h6>Headquarter & Processing Plant</h6><p>Jl. Margomulyo No.4E<br>Surabaya, Jawa Timur 60186<br>Telephone. +62 31 7491000\r\n</p>'),
(61, 376, 2, '<h4>Lamongan</h4><h6>Processing Plant & Office</h6><p>Jl. Panglima Sudirman No.40<br>Lamongan, Jawa Timur 62291<br>Telephone. +62 322 3326080\r\n</p>'),
(62, 383, 2, '<p>PT. Bumi Menara Internusa to be the leader of high nutritional south-east asian sea food exporter and to be the first choice for sea food products processing in the world.</p>'),
(63, 384, 2, '<p>To provide nutritional high quality sea food products, thus meeting multiple consumer health needs, in existing and new markets through widening our distribution network throughout Indonesia and the globe.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `shp_shipping_price`
--

CREATE TABLE `shp_shipping_price` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sl_slide`
--

CREATE TABLE `sl_slide` (
  `id` int(11) NOT NULL,
  `topik_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `date_input` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `insert_by` varchar(255) NOT NULL,
  `last_update_by` varchar(255) NOT NULL,
  `writer` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL,
  `image2` varchar(225) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sl_slide`
--

INSERT INTO `sl_slide` (`id`, `topik_id`, `image`, `active`, `date_input`, `date_update`, `insert_by`, `last_update_by`, `writer`, `sort`, `image2`) VALUES
(1, 0, 'f5fb8-fcs-1.jpg', 1, '0000-00-00 00:00:00', '2020-03-16 17:43:23', 'info@markdesign.net', 'info@markdesign.net', '', 1, 'c50ff-fcs-mobile.jpg'),
(2, 0, '0cee1-fcs-002.jpg', 1, '2020-07-06 15:13:20', '2020-07-06 15:13:20', 'info@markdesign.net', 'info@markdesign.net', '', 2, '0cee1-fcs-002_res.jpg'),
(3, 0, 'd56b6-fcs-003.jpg', 1, '2020-07-06 15:13:44', '2020-07-06 15:13:44', 'info@markdesign.net', 'info@markdesign.net', '', 3, 'd56b6-fcs-003_res.jpg'),
(4, 0, 'fef23-fcs-004.jpg', 1, '2020-07-06 15:14:00', '2020-07-06 15:14:00', 'info@markdesign.net', 'info@markdesign.net', '', 4, 'fef23-fcs-004_res.jpg'),
(5, 0, '70bd9-fcs-005.jpg', 1, '0000-00-00 00:00:00', '2020-07-06 15:24:38', 'info@markdesign.net', 'info@markdesign.net', '', 5, '401db-fcs-005_res.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sl_slide_description`
--

CREATE TABLE `sl_slide_description` (
  `id` int(11) NOT NULL,
  `slide_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `url_teks` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sl_slide_description`
--

INSERT INTO `sl_slide_description` (`id`, `slide_id`, `language_id`, `title`, `subtitle`, `content`, `url_teks`, `url`) VALUES
(3, 1, 2, 'slide 1', '', '', '', ''),
(4, 2, 2, 'slide 2', '', '', '', ''),
(5, 3, 2, 'slide 3', '', '', '', ''),
(6, 4, 2, 'slide 4', '', '', '', ''),
(8, 5, 2, 'slide 5', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_group`
--

CREATE TABLE `tb_group` (
  `id` int(11) NOT NULL,
  `group` varchar(50) NOT NULL,
  `aktif` int(11) NOT NULL,
  `akses` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_group`
--

INSERT INTO `tb_group` (`id`, `group`, `aktif`, `akses`) VALUES
(8, 'Administrator', 1, 0x613a33373a7b693a303b733a31363a2261646d696e2e757365722e696e646578223b693a313b733a31373a2261646d696e2e757365722e637265617465223b693a323b733a31373a2261646d696e2e757365722e757064617465223b693a333b733a31373a2261646d696e2e757365722e64656c657465223b693a343b733a31373a2261646d696e2e736c6964652e696e646578223b693a353b733a31383a2261646d696e2e736c6964652e637265617465223b693a363b733a31383a2261646d696e2e736c6964652e757064617465223b693a373b733a31383a2261646d696e2e736c6964652e64656c657465223b693a383b733a31363a2261646d696e2e62616e6b2e696e646578223b693a393b733a31373a2261646d696e2e62616e6b2e637265617465223b693a31303b733a31373a2261646d696e2e62616e6b2e757064617465223b693a31313b733a31373a2261646d696e2e62616e6b2e64656c657465223b693a31323b733a31393a2261646d696e2e73657474696e672e696e646578223b693a31333b733a31383a2261646d696e2e6d656d6265722e696e646578223b693a31343b733a31393a2261646d696e2e6d656d6265722e637265617465223b693a31353b733a31393a2261646d696e2e6d656d6265722e757064617465223b693a31363b733a31393a2261646d696e2e6d656d6265722e64656c657465223b693a31373b733a31373a2261646d696e2e6f726465722e696e646578223b693a31383b733a31383a2261646d696e2e6f726465722e637265617465223b693a31393b733a31383a2261646d696e2e6f726465722e757064617465223b693a32303b733a31383a2261646d696e2e6f726465722e64656c657465223b693a32313b733a31373a2261646d696e2e6f726465722e7072696e74223b693a32323b733a32313a2261646d696e2e73657474696e672e636f6e74616374223b693a32333b733a31393a2261646d696e2e73657474696e672e61626f7574223b693a32343b733a32303a2261646d696e2e63617465676f72792e696e646578223b693a32353b733a32313a2261646d696e2e63617465676f72792e637265617465223b693a32363b733a32313a2261646d696e2e63617465676f72792e757064617465223b693a32373b733a32313a2261646d696e2e63617465676f72792e64656c657465223b693a32383b733a31393a2261646d696e2e73657474696e672e686f77746f223b693a32393b733a31393a2261646d696e2e70726f647563742e696e646578223b693a33303b733a32303a2261646d696e2e70726f647563742e637265617465223b693a33313b733a32303a2261646d696e2e70726f647563742e757064617465223b693a33323b733a32303a2261646d696e2e70726f647563742e64656c657465223b693a33333b733a32303a2261646d696e2e64656c69766572792e696e646578223b693a33343b733a32313a2261646d696e2e64656c69766572792e637265617465223b693a33353b733a32313a2261646d696e2e64656c69766572792e757064617465223b693a33363b733a32313a2261646d696e2e64656c69766572792e64656c657465223b7d);

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu_akses`
--

CREATE TABLE `tb_menu_akses` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `action` blob NOT NULL,
  `sub_action` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_menu_akses`
--

INSERT INTO `tb_menu_akses` (`id`, `type`, `name`, `controller`, `action`, `sub_action`) VALUES
(22, 'admin', 'User', 'user', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(21, 'admin', 'Slide', 'slide', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(40, 'admin', 'Bank', 'bank', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(18, 'admin', 'Setting', 'setting', 0x613a313a7b733a353a22696e646578223b733a31373a22456469742053657474696e6720556d756d223b7d, 0x613a303a7b7d),
(39, 'admin', 'Member', 'member', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(38, 'admin', 'Order', 'order', 0x613a353a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b733a353a227072696e74223b733a353a225072696e74223b7d, 0x613a303a7b7d),
(32, 'admin', 'Contact Us', 'setting', 0x613a313a7b733a373a22636f6e74616374223b733a32323a2245646974205061676520487562756e6769204b616d69223b7d, 0x613a303a7b7d),
(13, 'admin', 'About Us', 'setting', 0x613a313a7b733a353a2261626f7574223b733a31303a22456469742041626f7574223b7d, 0x613a303a7b7d),
(37, 'admin', 'Category', 'category', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(36, 'admin', 'How To Order', 'setting', 0x613a313a7b733a353a22686f77746f223b733a31323a22486f7720546f204f72646572223b7d, 0x613a303a7b7d),
(30, 'admin', 'Products', 'product', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d),
(41, 'admin', 'Delivery Price', 'delivery', 0x613a343a7b733a353a22696e646578223b733a393a224c6973742044617461223b733a363a22637265617465223b733a31313a224372656174652044617461223b733a363a22757064617465223b733a31313a225570646174652044617461223b733a363a2264656c657465223b733a31313a2244656c6574652044617461223b7d, 0x613a303a7b7d);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `user_input` varchar(200) NOT NULL,
  `tanggal_input` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `initial` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `nama`, `pass`, `type`, `group_id`, `login_terakhir`, `aktivasi`, `aktif`, `user_input`, `tanggal_input`, `initial`, `image`) VALUES
(1, 'deoryzpandu@gmail.com', 'Deory Pandu', '564fda17f517ae04a86734c2b2341327ed4fd565', 'root', 0, '2015-12-30 08:16:30', 0, 1, '', '2014-02-10 03:17:36', 'deory', ''),
(30, 'info@markdesign.net', 'info markdesign', '564fda17f517ae04a86734c2b2341327ed4fd565', 'root', 1, '2019-03-29 04:09:38', 0, 1, '', '0000-00-00 00:00:00', 'Admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `to_toko`
--

CREATE TABLE `to_toko` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `login_terakhir` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `aktivasi` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `nama_toko` varchar(200) NOT NULL,
  `lokasi` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `to_toko`
--

INSERT INTO `to_toko` (`id`, `email`, `first_name`, `last_name`, `pass`, `login_terakhir`, `aktivasi`, `aktif`, `image`, `hp`, `address`, `city`, `province`, `postcode`, `nama_toko`, `lokasi`) VALUES
(1, 'deoryzpandu@gmail.com', 'Deory', 'Pandu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2014-11-07 15:32:14', 0, 1, 'a448d-calourette-woodland-creature-jewelry-1.jpg', 'HP', 'Address', 'City', 'Province', 'PostCode', 'JewelryShop', 'surabaya'),
(4, 'ibnu@markdesign.net', 'Ibnu', 'Fajar', '564fda17f517ae04a86734c2b2341327ed4fd565', '2014-11-07 15:32:25', 0, 1, '3e491-calourette-woodland-creature-jewelry-1.jpg', 'HP', 'Address', 'City', 'Province', 'PostCode', 'Toko Handoko', 'surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `to_toko_product`
--

CREATE TABLE `to_toko_product` (
  `id` int(11) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `to_toko_product`
--

INSERT INTO `to_toko_product` (`id`, `toko_id`, `product_id`) VALUES
(8, 1, 960),
(7, 1, 105),
(6, 1, 719),
(5, 1, 264),
(9, 1, 223),
(10, 1, 930),
(11, 1, 475),
(12, 1, 732),
(13, 4, 264),
(14, 4, 560),
(15, 4, 960),
(16, 4, 505),
(17, 4, 719),
(18, 4, 678),
(19, 4, 475),
(20, 4, 277);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `awal` int(11) NOT NULL,
  `akhir` int(11) NOT NULL,
  `trip` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `year`, `month`, `awal`, `akhir`, `trip`) VALUES
(1, 2016, 1, 1, 3, 'Surabaya'),
(4, 2016, 2, 8, 10, 'Singapore'),
(3, 2016, 2, 7, 10, 'Malaysia');

-- --------------------------------------------------------

--
-- Table structure for table `tt_text`
--

CREATE TABLE `tt_text` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tt_text`
--

INSERT INTO `tt_text` (`id`, `category`, `message`) VALUES
(1, 'admin', 'Produk'),
(2, 'admin', 'Pages'),
(3, 'admin', 'Orders'),
(4, 'admin', 'Customers'),
(5, 'admin', 'Promotions'),
(6, 'admin', 'Reports'),
(7, 'admin', 'General Setting'),
(8, 'admin', 'Data Edited'),
(9, 'admin', 'New Orders'),
(10, 'admin', 'New Customers'),
(11, 'admin', 'Payment Confirmation'),
(12, 'admin', 'Edit Profile'),
(13, 'admin', 'Change Password'),
(14, 'admin', 'Sign Out'),
(15, 'admin', 'Gallery'),
(16, 'admin', 'Slide Home'),
(17, 'admin', 'Toko'),
(18, 'admin', 'Slides'),
(19, 'admin', 'Product'),
(20, 'admin', 'Products'),
(21, 'admin', 'About Us'),
(22, 'admin', 'Contact Us'),
(23, 'admin', 'Trip'),
(24, 'admin', 'Trips'),
(25, 'admin', 'Slide'),
(26, 'admin', 'Healty'),
(27, 'admin', 'ge-ma'),
(28, 'admin', 'Blog/Artikel'),
(29, 'admin', 'Career'),
(30, 'admin', 'Home'),
(31, 'admin', 'Factory'),
(32, 'admin', 'News & Article'),
(33, 'admin', 'Lokasi Penjualan'),
(34, 'admin', 'Jadi Agen'),
(35, 'admin', 'Cara Membeli'),
(36, 'admin', 'PDF'),
(37, 'admin', 'Cara Belanja'),
(38, 'admin', 'Info Pengiriman'),
(39, 'admin', 'FAQ'),
(40, 'admin', 'Syarat & Ketentuan'),
(41, 'admin', 'How To Order'),
(42, 'admin', 'Event'),
(43, 'admin', 'Homepage'),
(44, 'admin', 'Brand'),
(45, 'admin', 'Become an Agent'),
(46, 'admin', 'Where to Buy'),
(47, 'admin', 'Tentang Kami'),
(48, 'admin', 'Belanja Online'),
(49, 'admin', 'Merek'),
(50, 'admin', 'Lokasi'),
(51, 'admin', 'Gallery Spotlight'),
(52, 'admin', 'Voucher Discount'),
(53, 'admin', 'Customer'),
(54, 'admin', 'Home Spotlight'),
(55, 'admin', 'Store Locator'),
(56, 'admin', 'Seen On'),
(57, 'admin', 'Merchant Partner'),
(58, 'admin', 'Lokasi Toko'),
(59, 'front', 'Contact'),
(60, 'front', 'Calcium Carbonate Products'),
(61, 'front', 'Process & Quality'),
(62, 'front', 'News & Articles'),
(63, 'front', 'BACK'),
(64, 'front', 'PROCESS QUALITY'),
(65, 'front', 'material sourcing'),
(66, 'front', 'oven & crush'),
(67, 'front', 'final screening'),
(68, 'front', 'dispatch'),
(69, 'front', 'Read More'),
(70, 'front', 'Quality'),
(71, 'front', 'Events'),
(0, 'admin', 'Blog'),
(0, 'admin', 'About');

-- --------------------------------------------------------
--
-- Structure for view `view_blog`
--
DROP TABLE IF EXISTS `view_blog`;

CREATE VIEW `view_blog`  AS  select `pg_blog`.`id` AS `id`,`pg_blog`.`topik_id` AS `topik_id`,`pg_blog`.`image` AS `image`,`pg_blog`.`active` AS `active`,`pg_blog`.`date_input` AS `date_input`,`pg_blog`.`date_update` AS `date_update`,`pg_blog`.`insert_by` AS `insert_by`,`pg_blog`.`last_update_by` AS `last_update_by`,`pg_blog`.`writer` AS `writer`,`pg_blog_description`.`id` AS `id2`,`pg_blog_description`.`blog_id` AS `blog_id`,`pg_blog_description`.`language_id` AS `language_id`,`pg_blog_description`.`title` AS `title`,`pg_blog_description`.`content` AS `content`,`pg_blog_description`.`quote` AS `quote` from (`pg_blog` join `pg_blog_description` on(`pg_blog`.`id` = `pg_blog_description`.`blog_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_category`
--
DROP TABLE IF EXISTS `view_category`;

CREATE VIEW `view_category`  AS  select `prd_category`.`id` AS `id`,`prd_category`.`parent_id` AS `parent_id`,`prd_category`.`sort` AS `sort`,`prd_category`.`image` AS `image`,`prd_category`.`type` AS `type`,`prd_category`.`data` AS `data`,`prd_category_description`.`id` AS `id2`,`prd_category_description`.`category_id` AS `category_id`,`prd_category_description`.`language_id` AS `language_id`,`prd_category_description`.`name` AS `name`,`prd_category_description`.`data` AS `data2` from (`prd_category` join `prd_category_description` on(`prd_category`.`id` = `prd_category_description`.`category_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_gallery`
--
DROP TABLE IF EXISTS `view_gallery`;

CREATE VIEW `view_gallery`  AS  select `gal_gallery`.`id` AS `id`,`gal_gallery`.`topik_id` AS `topik_id`,`gal_gallery`.`image` AS `image`,`gal_gallery`.`image2` AS `image2`,`gal_gallery`.`active` AS `active`,`gal_gallery`.`date_input` AS `date_input`,`gal_gallery`.`date_update` AS `date_update`,`gal_gallery`.`insert_by` AS `insert_by`,`gal_gallery`.`last_update_by` AS `last_update_by`,`gal_gallery`.`writer` AS `writer`,`gal_gallery`.`city` AS `city`,`gal_gallery`.`harga` AS `harga`,`gal_gallery`.`color` AS `color`,`gal_gallery`.`orientation` AS `orientation`,`gal_gallery_description`.`id` AS `id2`,`gal_gallery_description`.`gallery_id` AS `gallery_id`,`gal_gallery_description`.`language_id` AS `language_id`,`gal_gallery_description`.`title` AS `title`,`gal_gallery_description`.`sub_title` AS `sub_title`,`gal_gallery_description`.`sub_title_2` AS `sub_title_2`,`gal_gallery_description`.`content` AS `content` from (`gal_gallery` join `gal_gallery_description` on(`gal_gallery`.`id` = `gal_gallery_description`.`gallery_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_slide`
--
DROP TABLE IF EXISTS `view_slide`;

CREATE VIEW `view_slide`  AS  select `sl_slide`.`id` AS `id`,`sl_slide`.`topik_id` AS `topik_id`,`sl_slide`.`image` AS `image`,`sl_slide`.`active` AS `active`,`sl_slide`.`date_input` AS `date_input`,`sl_slide`.`date_update` AS `date_update`,`sl_slide`.`insert_by` AS `insert_by`,`sl_slide`.`last_update_by` AS `last_update_by`,`sl_slide`.`writer` AS `writer`,`sl_slide_description`.`id` AS `id2`,`sl_slide_description`.`slide_id` AS `slide_id`,`sl_slide_description`.`language_id` AS `language_id`,`sl_slide_description`.`title` AS `title`,`sl_slide_description`.`content` AS `content`,`sl_slide_description`.`url` AS `url`,`sl_slide`.`sort` AS `sort` from (`sl_slide` join `sl_slide_description` on(`sl_slide_description`.`slide_id` = `sl_slide`.`id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cs_customer`
--
ALTER TABLE `cs_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `factory`
--
ALTER TABLE `factory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gal_gallery`
--
ALTER TABLE `gal_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gal_gallery_description`
--
ALTER TABLE `gal_gallery_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gal_gallery_image`
--
ALTER TABLE `gal_gallery_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `me_member`
--
ALTER TABLE `me_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `or_order`
--
ALTER TABLE `or_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `or_order_history`
--
ALTER TABLE `or_order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `or_order_product`
--
ALTER TABLE `or_order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `or_order_status`
--
ALTER TABLE `or_order_status`
  ADD PRIMARY KEY (`order_status_id`);

--
-- Indexes for table `pg_blog`
--
ALTER TABLE `pg_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pg_blog_description`
--
ALTER TABLE `pg_blog_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_category`
--
ALTER TABLE `prd_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_category_description`
--
ALTER TABLE `prd_category_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_category_product`
--
ALTER TABLE `prd_category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_product`
--
ALTER TABLE `prd_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_product_attributes`
--
ALTER TABLE `prd_product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_product_color`
--
ALTER TABLE `prd_product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_product_description`
--
ALTER TABLE `prd_product_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_product_image`
--
ALTER TABLE `prd_product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_description`
--
ALTER TABLE `setting_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sl_slide`
--
ALTER TABLE `sl_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sl_slide_description`
--
ALTER TABLE `sl_slide_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `cs_customer`
--
ALTER TABLE `cs_customer`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `factory`
--
ALTER TABLE `factory`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `gal_gallery`
--
ALTER TABLE `gal_gallery`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gal_gallery_description`
--
ALTER TABLE `gal_gallery_description`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gal_gallery_image`
--
ALTER TABLE `gal_gallery_image`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `me_member`
--
ALTER TABLE `me_member`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `or_order`
--
ALTER TABLE `or_order`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `or_order_history`
--
ALTER TABLE `or_order_history`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `or_order_product`
--
ALTER TABLE `or_order_product`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `or_order_status`
--
ALTER TABLE `or_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pg_blog`
--
ALTER TABLE `pg_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pg_blog_description`
--
ALTER TABLE `pg_blog_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `prd_category`
--
ALTER TABLE `prd_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `prd_category_description`
--
ALTER TABLE `prd_category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `prd_category_product`
--
ALTER TABLE `prd_category_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prd_product`
--
ALTER TABLE `prd_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prd_product_attributes`
--
ALTER TABLE `prd_product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_product_color`
--
ALTER TABLE `prd_product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prd_product_description`
--
ALTER TABLE `prd_product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prd_product_image`
--
ALTER TABLE `prd_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=385;

--
-- AUTO_INCREMENT for table `setting_description`
--
ALTER TABLE `setting_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `sl_slide`
--
ALTER TABLE `sl_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sl_slide_description`
--
ALTER TABLE `sl_slide_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
